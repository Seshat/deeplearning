    var pointData = { 
        datasets: [
{
            pointRadius: 3,
            backgroundColor: window.chartColors.blue,
            data: [
            {
 x: 1.901429,
            y: 9.405278
 }, 
            {
 x: 1.463988,
            y: 8.483724
 }, 
            {
 x: 1.197317,
            y: 5.604382
 }, 
            {
 x: 0.312037,
            y: 4.716440
 }, 
            {
 x: 0.311989,
            y: 5.293080
 }, 
            {
 x: 0.116167,
            y: 5.826396
 }, 
            {
 x: 1.732352,
            y: 8.678787
 }, 
            {
 x: 1.202230,
            y: 6.798196
 }, 
            {
 x: 1.416145,
            y: 7.746678
 }, 
            {
 x: 0.041169,
            y: 5.038909
 }, 
            {
 x: 1.939820,
            y: 10.148210
 }, 
            {
 x: 1.664885,
            y: 8.464896
 }, 
            {
 x: 0.424678,
            y: 5.787302
 }, 
            {
 x: 0.363650,
            y: 5.188027
 }, 
            {
 x: 0.366809,
            y: 6.069072
 }, 
            {
 x: 0.608484,
            y: 5.123400
 }, 
            {
 x: 1.049513,
            y: 6.820876
 }, 
            {
 x: 0.863890,
            y: 6.199562
 }, 
            {
 x: 0.582458,
            y: 4.283860
 }, 
            {
 x: 1.223706,
            y: 7.967238
 }, 
            {
 x: 0.278988,
            y: 5.098018
 }, 
            {
 x: 0.584289,
            y: 5.757981
 }, 
            {
 x: 0.732724,
            y: 5.963584
 }, 
            {
 x: 0.912140,
            y: 5.321049
 }, 
            {
 x: 1.570352,
            y: 8.290410
 }, 
            {
 x: 0.399348,
            y: 4.855328
 }, 
            {
 x: 1.028469,
            y: 6.283129
 }, 
            {
 x: 1.184829,
            y: 7.393202
 }, 
            {
 x: 0.092901,
            y: 4.682753
 }, 
            {
 x: 1.215090,
            y: 9.531455
 }, 
            {
 x: 0.341048,
            y: 5.197723
 }, 
            {
 x: 0.130103,
            y: 4.647860
 }, 
            {
 x: 1.897771,
            y: 9.618867
 }, 
            {
 x: 1.931264,
            y: 7.875021
 }, 
            {
 x: 1.616795,
            y: 8.823870
 }, 
            {
 x: 0.609228,
            y: 5.887913
 }, 
            {
 x: 0.195344,
            y: 7.049275
 }, 
            {
 x: 1.368466,
            y: 7.913037
 }, 
            {
 x: 0.880305,
            y: 6.942462
 }, 
            {
 x: 0.244076,
            y: 4.697518
 }, 
            {
 x: 0.990354,
            y: 5.802383
 }, 
            {
 x: 0.068777,
            y: 5.349154
 }, 
            {
 x: 1.818641,
            y: 10.207855
 }, 
            {
 x: 0.517560,
            y: 6.343712
 }, 
            {
 x: 1.325045,
            y: 7.065746
 }, 
            {
 x: 0.623422,
            y: 7.273061
 }, 
            {
 x: 1.040136,
            y: 5.718557
 }, 
            {
 x: 1.093421,
            y: 7.867119
 }, 
            {
 x: 0.369709,
            y: 7.299582
 }, 
            {
 x: 1.939169,
            y: 8.826971
 }, 
            {
 x: 1.550266,
            y: 8.084499
 }, 
            {
 x: 1.878998,
            y: 9.736645
 }, 
            {
 x: 1.789655,
            y: 8.865488
 }, 
            {
 x: 1.195800,
            y: 6.036736
 }, 
            {
 x: 1.843748,
            y: 9.599808
 }, 
            {
 x: 0.176985,
            y: 3.468651
 }, 
            {
 x: 0.391966,
            y: 5.649490
 }, 
            {
 x: 0.090455,
            y: 3.351939
 }, 
            {
 x: 0.650661,
            y: 7.501916
 }, 
            {
 x: 0.777355,
            y: 5.548810
 }, 
            {
 x: 0.542698,
            y: 5.306033
 }, 
            {
 x: 1.657475,
            y: 9.785942
 }, 
            {
 x: 0.713507,
            y: 4.909656
 }, 
            {
 x: 0.561869,
            y: 5.913067
 }, 
            {
 x: 1.085392,
            y: 8.563319
 }, 
            {
 x: 0.281848,
            y: 3.238062
 }, 
            {
 x: 1.604394,
            y: 8.997816
 }, 
            {
 x: 0.149101,
            y: 4.707187
 }, 
            {
 x: 1.973774,
            y: 10.703144
 }, 
            {
 x: 1.544490,
            y: 7.396518
 }, 
            {
 x: 0.397431,
            y: 3.871837
 }, 
            {
 x: 0.011044,
            y: 4.555074
 }, 
            {
 x: 1.630923,
            y: 9.189753
 }, 
            {
 x: 1.413715,
            y: 8.491637
 }, 
            {
 x: 1.458014,
            y: 8.720491
 }, 
            {
 x: 1.542541,
            y: 7.947597
 }, 
            {
 x: 0.148089,
            y: 4.676522
 }, 
            {
 x: 0.716931,
            y: 6.443867
 }, 
            {
 x: 0.231738,
            y: 3.980863
 }, 
            {
 x: 1.726207,
            y: 11.044395
 }, 
            {
 x: 1.246596,
            y: 8.213622
 }, 
            {
 x: 0.661796,
            y: 4.794085
 }, 
            {
 x: 0.127117,
            y: 5.037904
 }, 
            {
 x: 0.621965,
            y: 4.891212
 }, 
            {
 x: 0.650367,
            y: 6.738185
 }, 
            {
 x: 1.459212,
            y: 9.536233
 }, 
            {
 x: 1.275115,
            y: 7.004663
 }, 
            {
 x: 1.774425,
            y: 10.286653
 }, 
            {
 x: 0.944430,
            y: 7.246070
 }, 
            {
 x: 0.239188,
            y: 5.539626
 }, 
            {
 x: 1.426490,
            y: 10.176262
 }, 
            {
 x: 1.521570,
            y: 8.319322
 }, 
            {
 x: 1.122554,
            y: 6.613927
 }, 
            {
 x: 1.541934,
            y: 7.736289
 }, 
            {
 x: 0.987591,
            y: 6.146963
 }, 
            {
 x: 1.045466,
            y: 7.059295
 }, 
            {
 x: 0.855082,
            y: 6.906398
 }, 
            {
 x: 0.050838,
            y: 4.429206
 }, 
            {
 x: 0.215783,
            y: 5.474532
 } 
		]
                               }                               ]
								};
var ctx = document.getElementById("chart1").getContext("2d");
window.myBar = new Chart(ctx, {
  type: "scatter",
  data: pointData,
  options: {
    responsive: true,
    legend: {
      display: false,
      	labels: {
      	  fontSize: 22
      	},
      	position:"bottom"
        },
  }
});   
