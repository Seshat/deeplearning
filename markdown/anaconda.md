---
author: PF Villard
title: Utilisation de python pour le deep learning
subtitle: 
progress: true
slideNumber: true
navigationMode: 'linear'
---

- [Installer d'Anaconda](#installer-danaconda)
- [Créer un environnement virtuel](#créer-un-environnement-virtuel)
- [Sélectionner l'environnement](#sélectionner-lenvironnement)
- [Installer les packages pour les TP](#installer-les-packages-pour-les-tp)
- [Utiliser python pendant les TP](#utiliser-python-pendant-les-tp)



## Installer d'Anaconda

- Télécharger et installer Anaconda à partir du [site web](https://www.anaconda.com/products/individual#Downloads)
  
![](img/ana0.jpg){height=300px}


## Ouvrir Anaconda<!-- omit in toc -->

![](img/ana1.jpg){height=300px}


## Créer un environnement virtuel

- Ouvrir **Spyder** dans **Anaconda**
- Création d'un environnement virtuel :
```
conda create -n deepLearningEnv anaconda -y
```
- Attendre (cela peut aller jusqu'à 1 heure)
  
![](img/ana2.jpg){height=300px}


## Sélectionner l'environnement 
- Sélectionner cet environnement dans **Anaconda**
- Ou en ligne de commande
```
conda activate deepLearningEnv
```
- Pour vérifier l'environnement utilisé :
```
    import sys
print (sys.prefix)
```
<!-- sur mac : conda create -n deepLearningEnv python=3.6 anaconda -y -->

![](img/ana3.jpg){height=300px}


## Installer les packages pour les TP
- Télécharger le fichier avec la liste des package [ici](test/requirements.txt)
- Dans la **console**, aller dans le dossier ou se trouve ce fichier
```
cd ~/GitLab/cours/deeplearning/test/
```
- Installer les packages nécessaires pour ce cour :
```
conda install --yes --file requirements.txt
```
- Attendre (cela peut aller jusqu'à 1 heure)
  

## Utiliser python pendant les TP
- Activer l'environnement virtuel ```deepLearningEnv```
- Ouvrir ***Jupyter Notebook***

![](img/ana1.jpg){height=300px}


## Utilisation <!-- omit in toc -->
- Chercher et ouvrir le fichier correspondant à l'exercice
- D'abord, tester avec le fichier [test.ipynb](test/tests.ipynb) (à télécharger)
      - Parcourir l'arborescence et l'**ouvrir**

![](img/ana4.jpg){height=300px}


## Exemple de fichier Jupyter<!-- omit in toc -->
  - Un fichier jupyter ressemble à cela :

![](img/ana5.jpg){height=300px}


## Exemple de fichier Jupyter<!-- omit in toc -->

<iframe src='test/tests.html' height="500" width="1000" ></iframe>

## Si ça ne marche pas -> Docker <!-- omit in toc -->

- **Installer** [Docker]( https://www.docker.com/get-started)
- **Exécuter** Docker (Accepter les termes si besoin)
- Dans une **ligne de commande** :
```
sudo docker pull jupyter/tensorflow-notebook
```
ou si votre réseau wifi est faible :
```
sudo docker pull jtensorflow/tensorflow:latest-jupyter
```
- Attendre la fin du téléchargement et de l’extraction
- **Lancer le docker** téléchargé :
```
sudo docker run -it -p 8888:8888  jupyter/tensorflow-notebook
```
ou
```
sudo docker run -it -p 8888:8888 tensorflow/tensorflow:latest-jupyter
```


## Si ça ne marche pas -> Docker (suite) <!-- omit in toc -->

- Copier/coller l’adresse indiquée dans le terminal commençant par 127.0.0.1 dans un **navigateur**
- Passer par **upload** pour mettre les fichiers de données ou les fichiers jupyter (format .ipynb)
- Si besoin d’installer un **autre package**, utiliser 
```
!pip install nom du package
```
Example : 
```
!pip install sklearn
```