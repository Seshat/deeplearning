---
author: PF Villard
title: Deep Learning
---
## Plan du cours
- [Les fondamentaux](IA1.html)
- [Perceptron multicouche](IA2.html)
- [CNN](IA3.html)
- [Apprentissage par renforcement](IA4.html)

## Présentation de l'enseignant

Une présentation de l'enseignant ce trouve [ici](http://courshtml.iutsd.univ-lorraine.fr/pf.html#/pf)

## Références

- Cours en html
- Images :
  -  [Wikipedia](https://en.wikipedia.org)
  -  [Machine Learning for Artists](https://ml4a.github.io/)
- D'après :
  - [Machine Learning for Artists](https://ml4a.github.io/)
  - [Deep Learning avec TensorFlow](https://ulysse.univ-lorraine.fr/permalink/33UDL_INST/1u932an/alma991010388389705596) : mise en oeuvre et cas concrets, Aurélien Géron, Dunod - 2017

## Introduction

- Intelligence artificielle, apprentissages automatique et profond
![](https://ecoinfo.cnrs.fr/wp-content/uploads/2019/10/ia_ml_dl-731x365.png)


## Introduction

- Processus d’apprentissage profond supervisé
![](https://ecoinfo.cnrs.fr/wp-content/uploads/2019/10/archi_appr-731x355.png)

<!-- pandoc -t revealjs -s -o index.html intro.md -V revealjs-url=./reveal.js  -V transition=cube  -f markdown+smart -V theme=beige --mathjax --css slide.css -->

