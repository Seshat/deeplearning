---
author: PF Villard
title: Deep Learning
subtitle: Perceptron multicouche
progress: true
slideNumber: true
navigationMode: 'linear'
date: \today
---
<!-- pandoc -t revealjs -s -o IA2.html IA2.md -V revealjs-url=https://revealjs.com   -V transition=cube  -f markdown+smart -V theme=beige --mathjax  --css slide.css  --include-after-body='debut.txt'-->
- [De la biologie à l’artificiel](#de-la-biologie-à-lartificiel)
- [Calcul logique avec neurones](#calcul-logique-avec-neurones)
- [Perceptron](#perceptron)
- [Perceptron multicouche](#perceptron-multicouche)

## De la biologie à l’artificiel  <!-- {data-background=#8158589c} -->

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Historique<!-- omit in toc -->

- décrits pour la première fois en **1943** (par Warren McCulloch et Walter Pitts)
      - problèmes de logique propositionnelle
- 1960-1980 : promesse pas tenue 
- 1980 : nouvelles **architectures** de réseaux
- 1990 : des alternatives comme **SVM**
- Récemment : à la mode 

![](https://1.bp.blogspot.com/-u59IVaKus7U/WhIMOtEpHGI/AAAAAAAAr9Q/S8ChBoRRJtoX8jv6T_fiO7ELmOW4PJWAQCLcBGAs/s1600/1.JPG){height=200px}


## Neurones biologiques<!-- omit in toc -->

- Dans les cortex cérébraux, constitués de 
  - **corps cellulaire** (noyau+éléments)
  - Des prolongements appelés **dendrites**
  - Un très long prolongement appelé **axone**
      - décomposé en ramifications
      - se terminent par des **synapses**
      - **reliées** à d’autres neurones

![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Blausen_0657_MultipolarNeuron.png/640px-Blausen_0657_MultipolarNeuron.png){height=200px}
![](https://github.com/ml4a/ml4a.github.io/blob/master/images/neuron-simple.jpg?raw=true){height=200px}

## Fonctionnement <!-- omit in toc -->

- courtes impulsions, appelées **signaux**
- si neurone reçoit (pdt qq mns) un nombre suffisant de signaux :
    - déclenche ses propres signaux
- organisés en un vaste réseau de **milliards** de neurones
  
![](https://upload.wikimedia.org/wikipedia/commons/5/5b/Cajal_cortex_drawings.png){height=200px}

## Calcul logique avec neurones 

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## neurone artificiel<!-- omit in toc -->

- premier neurone artificiel
    - plusieurs entrées **binaires** (**active**/**inactive**) 
    - une sortie **binaire**
- active sa sortie lorsque nombre entrées actives > un seuil
- calcule n’importe quelle proposition logique
- Exemple :

![](https://github.com/ml4a/ml4a.github.io/blob/master/images/neuron-artificial.png?raw=true){height=150px}

 - **&rarr;** Le neurone est activé si un ou plusieurs neurone $X_i$ sont activés

## Perceptron

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Historique<!-- omit in toc -->

- inventé en 1957 par F.Rosenblatt
- architecture simple
- appelé unité linéaire à seuil (**LTU**, Linear Threshold Unit)
- Les entrées et la sortie sont à présent des nombres 

![](https://news.cornell.edu/sites/default/files/styles/story_thumbnail_xlarge/public/2019-09/0925_rosenblatt_main.jpg?itok=BCWmlVvO){height=200px}

## Fonctionnement<!-- omit in toc -->

- chaque connexion en entrée possède un poids
  - LTU calcule une somme pondérée des entrées
$$
 z = w_1 x_1 + w_2 x_2 + … + w_n x_n
$$
  - il applique une fonction échelon (**step**)
$$
hw(x) = step(z)
$$

![](https://miro.medium.com/max/2870/1*n6sJ4yZQzwKL9wnF5wnVNg.png){height=200px}

## Fonction échelon<!-- omit in toc -->

- Plus répandue : la fonction de **Heaviside** 
$$
Heaviside(z)=
\begin{cases}
0 & \text{si $z<0$} \\
1 & \text{si $z\geq 0$} 
\end{cases}
$$
- La fonction **signe** est parfois utilisée à la place
$$
signe(z)=
\begin{cases}
-1 & \text{si $z<0$} \\
0 & \text{si $z=0$} \\
1 & \text{si $z>0$} 
\end{cases}
$$

## Composition du perceptron<!-- omit in toc -->

- constitué d’une seule couche de LTU
- chaque neurone étant connecté à **toutes** les entrées
- connexions avec neurones intermédiaires = **neurones d’entrée**


  
## Comment entraîne-t-on un perceptron ?<!-- omit in toc -->
- règle de Hebb : si un neurone biologique déclenche souvent un autre neurone, alors la connexion entre ces deux neurones se renforce
- perceptron reçoit une instance d’entraînement à la fois
-  effectue ses prédictions.
-  Pour chaque neurone de sortie qui produit une prédiction erronée, il renforce les poids des connexions liées aux entrées qui auraient contribué à la prédiction juste.

## Couche cachée<!-- omit in toc -->

<iframe src="https://ml4a.github.io/demos/f_mnist_1layer/" width="1200" height="780" style="border: none;"></iframe>

## Entraînement<!-- omit in toc -->

<iframe src="
https://ml4a.github.io/demos/forward_pass_mnist/" width="1200" height="780" style="border: none;"></iframe>

## Exercices TD<!-- omit in toc --> 

Comprendre le perceptron : [JupyterNotebook](../jupyterNotebook/TD2/TD2.ipynb)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)



## Exercice<!-- omit in toc -->

- Comment prédire à quelle classe appartient une fleur de longueur et largeur de sépales données ?
- Comment tracer les fonctions d'activation classiques ?

**&rarr; TP !**

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## Perceptron multicouche

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Composition<!-- omit in toc -->

- une **couche d’entrée** (&rarr; distribuer les entrées aux neurones de la couche suivante)
- une ou plusieurs couches de LTU appelées **couches cachées**
- une dernière couche de LTU appelée **couche de sortie**

![](https://upload.wikimedia.org/wikipedia/commons/a/a9/Perceptron_4layers.png){height=200px}

- Si + d'une couche cachée : **Deep Neural Network (DNN)**

## Rétropropagation<!-- omit in toc -->

- Utilisé pour entrainer un réseau de neurones
- Utilise la méthode de descente de gradient
- Pour une donnée d’entraînement :
    - calcule la sortie de chaque neurone dans chaque couche consécutive
    - mesure ensuite l’erreur de sortie du réseau
    - détermine pour chaque neurone la contribution à l’erreur
- Processus se poursuit jusqu’à ce que l’algorithme atteigne la couche d’entrée = mesure gradient d’erreur sur tous les poids des connexions du réseau en rétropropageant le gradient d’erreur

## En résumé<!-- omit in toc -->
- pour chaque instance d’entraînement :
    - effectuer une prédiction (passe vers l’avant)
    - mesure l’erreur, traverse chaque couche en arrière pour mesurer la contribution à l’erreur de chaque connexion (passe vers l’arrière)
    - se termine en ajustant légèrement les poids des connexions de manière à réduire l’erreur (étape de descente de gradient).


## Exercices TD<!-- omit in toc --> 

Comprendre le perceptron multicouche : [JupyterNotebook](../jupyterNotebook/TD2/TD2.ipynb)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)


## Exercice<!-- omit in toc -->

- Entrainement de Perceptron avec Tensorflow
- Utilisation du réseau de neurones

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## Régler les hyperparamètres d’un réseau de Neurones<!-- omit in toc -->

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## hyperparamètres à ajuster<!-- omit in toc -->
- topologie de réseau (interconnexions des neurones)
- nombre de couches
- nombre de neurones par couche
- type de fonction d’activation (pour chaque couche)
- logique d’initialisation des poids
- etc.

## Nombre de couches cachées<!-- omit in toc -->
- commencer avec une seule couche
- Plusieurs couches **&rarr;** efficacité paramétrique **&rarr;** fonctions complexes
- Pour de nombreux problèmes : une ou deux couches cachées
- Pour problèmes + complexes, &nearr; progressivement nombre de couches cachées jusqu'à **surajustement**

## Fonction d’activation<!-- omit in toc -->

- ReLU dans les couches cachée &rarr; rapide
- softmax dans couche d'entrée si mutuellement exclusives

<div  style="width: 75%; margin: 0 auto">
<canvas id="chart25"></canvas>
</div>
<!-- ![](https://raw.githubusercontent.com/ml4a/ml4a.github.io/master/images/sigmoid.png){height=300px}
![](https://raw.githubusercontent.com/ml4a/ml4a.github.io/master/images/relu.png){height=300px} -->
