---
author: PF Villard
title: Deep Learning
subtitle: CNN
progress: true
slideNumber: true
navigationMode: 'linear'
---
<!-- pandoc -t revealjs -s -o IA3.html IA3.md -V revealjs-url=./reveal.js  -V transition=cube  -f markdown+smart -V theme=beige --mathjax   --css slide.css  --include-after-body='debut.txt'-->

- [Architecture du cortex visuel](#architecture-du-cortex-visuel)
- [Couche de convolution](#couche-de-convolution)
- [Pipeline complet](#pipeline-complet)
- [Augmentation de données](#augmentation-de-données)
- [Exemple](#exemple)
- [Validation croisée](#validation-croisée)
- [Base pré-entraînée](#base-pré-entraînée)
- [Architectures de CNN](#architectures-de-cnn)
- [VGG19](#vgg19)
- [Mise en production](#mise-en-production)
- [Conclusion](#conclusion)


## Présentation<!-- omit in toc -->

- Victoire de Deep Blue sur Garry Kasparov &rarr; **1996**
- Repérer un chat sur une photo &rarr; **?**
- &rarr; Image traitée par **modules sensoriels** spécialisés du cerveau
- &rarr;  largement prétraitée + subjectif
  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Cat_poster_1.jpg/1920px-Cat_poster_1.jpg){height=200px}


## Historique <!-- omit in toc -->
- CNN: Convolutional Neural Network
- Apparu début 1980
- Croissance grâce à l'évolution de la technologie
- Domaine :
    - perception visuelle
    - reconnaissance vocale 
    - traitement automatique du langage naturel

![](https://i1.wp.com/imalogic.com/blog/wp-content/uploads/2016/04/PRINCIPE.gif){height=200px}

## Architecture du cortex visuel

<img src="https://hbr.org/resources/images/article_assets/2021/01/Jan21_25_769779943.gif" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Expérience de Hubel & Wiesel (1960)<!-- omit in toc -->
 - <a href="https://en.wikipedia.org/wiki/David_H._Hubel">David Hubel</a> et<a href="https://en.wikipedia.org/wiki/Torsten_Wiesel">Torsten Wiesel</a> sur la structure du cortex visuel
 - certains neurones réagissent uniquement aux images de lignes horizontales
 - d’autres réagissent uniquement aux lignes ayant d’autres orientations
 - certains neurones ont des champs récepteurs plus larges et ils réagissent à des motifs plus complexes
    - &rarr; neurones de plus haut niveau se fondent sur sorties neurones voisins de plus bas niveau
  
 ![](https://ml4a.github.io/images/figures/hubel-wiesel.jpg){height=200px}
 
## Physiologie<!-- omit in toc -->

- **neocognitron** (1980) &rarr; **réseau de neurones convolutifs** (1998) par Yann LeCun
    - &rarr; architecture LeNet-5  
    - &rarr; numéros de chèques écrits à la main
- Architecture :
  - couches intégralement connectées (FC, **fully connected**) 
  - fonctions d’activation = **sigmoïdes**
      - **+** couches de **convolution**
      - **+** couches de **pooling**
  
![](https://ml4a.github.io/images/figures/lenet.png){height=200px}

## Couche de convolution

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Principe<!-- omit in toc -->

- neurones pas connectés à chaque pixel de l’image d’entrée
- neurones d'une couche de convolution : connectés aux neurones à l’intérieur d’un **petit rectangle** de la première couche
    - &rarr; se focalise sur des caractéristiques de bas niveau
    - &rarr; assemble en caractéristiques de plus haut niveau

![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Typical_cnn.png/614px-Typical_cnn.png){height=150px}
![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAbIISsSKiGEBYik-le-er4RixPpChoseFueBkJXc5gmTiph34&s){height=150px}

## Zero padding<!-- omit in toc -->

- Chaque couche = même hauteur et même largeur
- **&rarr;** on ajoute des zéros autour des entrées (marge) 
- **=** remplissage par zéros (**zero padding**)
- **=** ajouter une marge de zéros
  
![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlsG469ya0QH87uikEs1cOx6EqgN7xin2QuKax_505iRfFraNe&s){height=200px}


## Démo<!-- omit in toc -->
<iframe src="https://ml4a.github.io/demos/convolution/" width="900" height="780" style="border: none;"></iframe>

## Filtres<!-- omit in toc -->

- poids d’un neurone ≈ petite image (taille = champ récepteur)
- ensemble de poids possibles = **filtres** (ou noyaux de convolution)
  
![](https://wizardforcel.gitbooks.io/scikit-and-tensorflow-workbooks-bjpcjp/pics/filters.png){height=200px}

- Exemple : 
    - filtres horizontal et vertical
    - lignes **mises en valeur** et le reste **flou** 


___

<iframe src="
https://ml4a.github.io/demos/convolution_all/" width="1200" height="780" style="border: none;"></iframe>


## Empiler plusieurs caractéristiques<!-- omit in toc -->

- chaque couche de convolution = plusieurs cartes de caractéristiques de taille égale
- une carte de caractéristiques = mêmes paramètres (poids et terme constant)
- plusieurs filtres simultanément appliqués par couche
- **&rarr;** détecter plusieurs caractéristiques
- **+** images d’entrée = multiples sous-couches &rarr; **canaux de couleur**

![](https://wizardforcel.gitbooks.io/scikit-and-tensorflow-workbooks-bjpcjp/pics/stacking-feature-maps.png){height=250px}

## Exercices TD<!-- omit in toc --> 

Comprendre les couches de convolution  [JupyterNotebook](../jupyterNotebook/TD3/CNN.ipynb)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)


## Pipeline complet

- Pixels de l'image brute à gauche (ex : 256x256x3)
- Ensuite : filtres de convolutions
  -  Petits filtres qui glissent sur l'image
  -  Excités par différentes caractéristiques de l'image : 
     -  Petit bord horizontal
     -  Régions de couleur rouge
     -  etc. 
  -  Si 10 filtres : image originale (256,256,3) &rarr; (256,256,10)
  <!-- dans laquelle nous avons supprimé les informations de l'image originale et ne conservons que les 10 réponses de nos filtres à chaque position de l'image. C'est comme si les trois canaux de couleur (rouge, vert, bleu) étaient maintenant remplacés par 10 canaux de réponse de filtre (je les montre le long de la première colonne immédiatement à droite de l'image dans le gif ci-dessus). -->

![](http://karpathy.github.io/assets/selfie/gif2.gif)
  
## Besoin en mémoire <!-- omit in toc -->

-  besoin d’une grande quantité de **RAM**
      -  **&rarr;** entraînement :
            - **&rarr;** rétropropagation utilise toutes les valeurs intermédiaires calculées pendant la passe en avant
-  Exemple :
      -  Données :
            - filtres de **5×5**
            - **200 **cartes de caractéristiques de taille 150×100
            - pas de 1
            - marge de zéros
            - image RGB (**trois canaux**) de **150×100**
      - **&rarr;** nombre de paramètres :
           - (5 × 5 × 3 + 1) × 200 = 15 200
           - 200 cartes de caractéristiques = 150 × 100 neurones
           - chacun de ces neurones calcule somme pondérée de ses 5 × 5 × 3 = 75 entrées
           - **&rarr;**  total de **225 millions** de multiplications de nombres à virgule flottante
           - **&rarr;** *faible / fully connected*

## Couches de pooling <!-- omit in toc -->

- objectif = sous-échantillonner (rétrécir) 
    - **&rarr;** charge de calcul &searr;
    - **&rarr;** utilisation de la mémoire &searr;
    - **&rarr;** nombre de paramètres &searr; (risque de surajustement &searr;)
- Paramètres :
    - taille
    - pas
    - type de remplissage
    - **&rarr;** pas de poids (se contente d’agréger)

![](https://upload.wikimedia.org/wikipedia/commons/e/e9/Max_pooling.png){height=200px}
![](../img/pooling.png){height=200px}

## Exercices TD<!-- omit in toc --> 

Comprendre les couches de pooling  [JupyterNotebook](../jupyterNotebook/TD3/CNN.ipynb)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## Augmentation de données

- &nearr; artificiellement jeu d'entraînement
- Nécessaire si **base de données trop petite**
- Permet de **régulariser** le modèle (&searr; surajustement)
- = Décaler, pivoter, redimensionner, changer le contraste, générer des images en miroir, etc.
- **Générer à la volée** à l'entraînement &rarr; RAM &searr;

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/An_Example_of_Data_Augmentation_via_Augmentor.png/220px-An_Example_of_Data_Augmentation_via_Augmentor.png)

## Exemple

![](../img/augm.png)

## Code TensorFlow  <!-- omit in toc -->

```
img_gen = ImageDataGenerator(
    width_shift_range=XXX,
    height_shift_range=XXX,
    rescale=1. / 255,
    shear_range=XXX,
    rotation_range=XXX,
    zoom_range=XXX,
    horizontal_flip=True ou False)
```

![https://stepup.ai/exploring_data_augmentation_keras/](https://stepup.ai/content/images/size/w300/2020/07/banner.002.jpeg)


## Synthèse de données  <!-- omit in toc -->

- Même idée que précédemment
- Utilisation de génération par images de synthèses

<iframe src="https://homepages.loria.fr/PFVillard/panneaux-routiers/PT4.html"  height="500" width="1000"></iframe>

___

<iframe src="https://homepages.loria.fr/PFVillard/grille/index.html"  height="500" width="1000"></iframe>




## Validation croisée

![](../img/dataAugmentation1.png){height=200px}

- A faire si la base de donnée est petite

![](../img/validation.png){height=200px}

## Validation croisée exemple <!-- omit in toc -->

| <img  height="300" src="../img/dataAugmentation2.png"> |  <img height="300" src="../img/dataAugmentation4.png"> |
|----------|:-------------:|
| Sans augmentation | Avec |


## Base pré-entraînée

- Toujours pour une base de donnée petite
- Utilisation de poids issus d'une grande base **étiquetée**
- Caractéristiques de bas niveau déjà appris
- Exemple : poids d'[ImageNet](http://image-net.org/update-mar-11-2021.php)
- Compris dans [Kerras - TensorFlow](https://keras.io/api/applications/)

## Sans pré-entraînement  <!-- omit in toc --> 

<div class="container">
<div class="col">
![](../img/noPreTraining1.png){ width=70%}  
</div>
<div class="col">
![](../img/noPreTraining2.png){ width=70% }  
</div>
</div>


## Architectures de CNN

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Architectures classiques <!-- omit in toc -->

- Empilement de :
    -  une ou deux couches de **convolution**
    -  une couche de **pooling**
    -  une ou deux couches de **convolution**
    -  une autre couche de **pooling**
    -  ainsi de suite**...**
- **&rarr;** les cartes de caractéristiques rétrécissent au fur et à mesure que l’on traverse le réseau
- **&rarr;** nombre de cartes de caractéristiques  &nearr; 
- sommet de la pile : quelques couches **fully connected** 
- couche finale = prédiction **&rarr;**  couche softmax
  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Typical_cnn.png/395px-Typical_cnn.png){height=150px}

## LeNet-5 (1998) <!-- omit in toc -->

- La + connue des architectures de CNN
- Utilisée pour la reconnaissance des chiffres écrits à la main (MNIST)
- détails :
    -  images MNIST font 28×28 pixels
    -  mean-pooling : moyenne + multiplie résultat par coefficient + ajoute un terme constant + applique la fonction d’activation
    -  couche de sortie particulière basée distance euclidienne 

<a href="http://yann.lecun.com/exdb/lenet/">**DEMO**</a>

## AlexNet (2012)<!-- omit in toc -->

- plus large et profond que LeNet
- empiler des couches de convolution directement les unes au-dessus des autres
- Pour diminuer le surajustement, les auteurs ont employé deux techniques de régularisation
- au cours de l’entraînement, appliquer un dropout
- augmentation des données en décalant aléatoirement les images d’entraînement, en les retournant horizontalement et en changeant les conditions d’éclairage
- utilise également une étape de normalisation immédiatement après l’étape ReLU des couches C1 et C3, appelée normalisation de réponse locale
  
## GoogLeNet (2014) 1/2<!-- omit in toc -->

- beaucoup plus profond que les CNN précédents
- &larr; sous-réseaux nommés **modules d'inception** = couche de convolution pour cartes de caractéristiques avec motifs complexes à différentes échelles
- couches de convolution utilisent la fonction d’activation ReLU

![](https://wizardforcel.gitbooks.io/scikit-and-tensorflow-workbooks-bjpcjp/pics/inception.png)

## GoogLeNet (2014) 2/2<!-- omit in toc -->
- architecture de CNN GoogLeNet =  **9** modules d'inception
![](https://wizardforcel.gitbooks.io/scikit-and-tensorflow-workbooks-bjpcjp/pics/googlenet.png)

## VGG19

- **19 couches** :
  - 16 convolution layers, 3 Fully connected layer, 5 MaxPool layers et 1 SoftMax layer
  
![](../img/VGG19.png)

## Mise en production

- Utilisation de ``TensorFlow.js``
- **Exporter** le modèle ([documentation](https://www.tensorflow.org/js/tutorials/conversion/import_keras))
- **Importer** le modèle :
```
const model = tf.loadLayersModel('model.json');
```
- **Lire l'image** à partir d'une page web, d'un dossier ou de la webcam
```
let image = document.getElementById('monImage');
```

## Mise en production (2)<!-- omit in toc -->
- **Ouvrir** le modèle
```
model.then(function (res) {
```    
- **Convertir** l'image dans le format TensorFlow  
```
    let example = tf.browser.fromPixels(image).expandDims();
```
- Changer la **taille**
```
    example = tf.image.resizeBilinear(example, [150, 150]).div(tf.scalar(255))
```    
- **Caster** en float
```
    example = tf.cast(example, dtype = 'float32');
```    
- **Prédire** la classe
```
    let prediction = res.predict(example);
    figurineClass=Math.round (prediction.dataSync());
```


## Mise en production (3)<!-- omit in toc -->
- Ajouter le résultat au HTML
```
    let tag = document.createElement("p");
    let text = document.createTextNode("Prediction : ");
    tag.appendChild(text);
    let text2 = document.createTextNode(classes[figurineClass]);
    tag.appendChild(text2);
    let element = document.getElementById(tags[i]);
    element.appendChild(tag);
}, function (err) {
    console.log(err);
});
```

[Lien 1](https://members.loria.fr/PFVillard/files/asterix/Ast.html) et [Lien 2](https://members.loria.fr/PFVillard/files/asterix/)



## Demo  <!-- omit in toc -->

<iframe src="https://demos.marcelle.dev/sketch-online/" width="1200px" height="700px">
</iframe>



## Conclusion

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Conclusion <!-- omit in toc -->

- Les évolutions sont rapides
- Nouvelles sortes d’architectures proposées chaque année
- Les CNN sont :
    - de + en + profonds
    - de + en + légers
        - &larr; de - en - de paramètres
- [Lien vers un super article](../pdf/TSP_FDMP_21726.pdf)

## EXERCICES<!-- omit in toc -->

[Ici](../jupyterNotebook/TD3/TD3.ipynb)