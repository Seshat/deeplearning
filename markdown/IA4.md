---
author: PF Villard
title: Deep Learning 
subtitle: Apprentissage par renforcement
progress: true
slideNumber: true
navigationMode: 'linear'
---
<!-- pandoc -t revealjs -s -o IA4.html IA4.md -V revealjs-url=./reveal.js  -V transition=cube   -f markdown+smart -V theme=beige --mathjax    --css slide.css --include-after-body='debut.txt'-->

- [Introduction](#introduction)
- [Apprendre à optimiser les récompenses](#apprendre-%c3%a0-optimiser-les-r%c3%a9compenses)
- [Recherche de politique](#recherche-de-politique)
- [Exercice](#exercice)
- [Politique par réseau de neurones](#politique-par-r%c3%a9seau-de-neurones)
- [Evaluation des actions](#evaluation-des-actions)
- [Gradients de politique](#gradients-de-politique)
- [Processus de décision markoviens](#processus-de-d%c3%a9cision-markoviens)
- [Apprentissage Q par approximation et apprentissage Q profond](#apprentissage-q-par-approximation-et-apprentissage-q-profond)
- [Exercices](#exercices)

## Introduction

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Historique <!-- omit in toc -->

- Existe depuis les années **1950**
- Surtout utilisé dans le **jeu**
- **2013** : **DeepMind** l'utilise pour apprendre n’importe quel jeu Atari
  - &rarr; bat tous les humains
  - &rarr; ne connait que les pixels bruts en entrée
  - &rarr; sans connaissances préalables des règles du jeu
- mai **2017** : **AlphaGo** bat le champion du monde de go

![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Atari-2600-Wood-4Sw-Set.jpg/300px-Atari-2600-Wood-4Sw-Set.jpg){height=150px}
![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/FloorGoban.JPG/300px-FloorGoban.JPG){height=150px}

## Apprendre à optimiser les récompenses

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Définition  <!-- omit in toc -->

-  Apprentissage par renforcement : 
   -  un **agent** logiciel procède à des **observations** et réalise des **actions** au sein d’un **environnement**
  - en retour, il reçoit des **récompenses**
  - Son objectif est d’apprendre à faire des **actions** de façon à maximiser les récompenses espérées sur le long terme. 

![](https://i1.wp.com/blog.mr-int.ch/wp-content/uploads/2017/03/carotte-baton.png?fit=600%2C600&ssl=1){height=200px}

## Exemples  <!-- omit in toc -->

1. **Agent** = programme qui contrôle un robot marcheur
   - **environnement** = monde réel
   - **Observation** = capteurs (caméras, capteurs tactiles, ...)
   - **Actions** = envoyer des signaux pour activer les moteurs
   - **récompenses** = 
     -  **positives** approche de la destination visée
     -  **négatives** perd du temps / va dans mauvaise direction / tombe  
2. **Agent** = programme qui contrôle Ms. Pac-Man
   - **environnement** = simulation du jeu Atari
   - **Observation** = captures d’écran
   - **Actions** = neuf positions possibles du joystick
   - **récompenses** = points obtenus au jeu
3. **Agent** = déplacement d’un objet physique (thermostat intelligent)
   - **récompenses** = 
     -  **positives** Atteinte de la température visée / économisant de l’énergie
     -  **négatives** température doit être ajustée manuellement

## Recherche de politique

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Politique  <!-- omit in toc -->

- **Politique** = algorithme que l’agent logiciel utilise pour déterminer ses actions
- **&rarr;** tout algorithme imaginable et n’est pas nécessairement déterministe
- **Ex :** un réseau de neurones qui prend en entrée des observations et produit en sortie l’action à réaliser

![](https://static.bookstack.cn/projects/hands_on_Ml_with_Sklearn_and_TF/images/chapter_16/16-2.png){height=300px}

## Exemple : robot aspirateur  <!-- omit in toc -->

- **récompense** = volume de poussière ramassée en 30 minutes
- **politique** = 
  - avancer à chaque seconde (probabilité **p**) ou tourner aléatoirement vers la gauche ou la droite (probabilité **1 –p**)
  - angle de rotation avec valeur aléatoire située (entre **–r** et **+r**)
  
-  **&rarr;** politique aléatoire, finira par arriver à tout endroit

- question : quelle quantité de poussière va-t-il ramasser en 30 minutes ?

![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/France_in_XXI_Century._Electric_scrubbing.jpg/800px-France_in_XXI_Century._Electric_scrubbing.jpg){height=250px}

## Entrainement (1/2)  <!-- omit in toc -->

- **2** paramètres de politique à ajuster : la probabilité **p** et la plage de l’angle **r**
- **Solution 1** : essayer de nombreuses valeurs différentes pour ces paramètres et de retenir la combinaison qui affiche les meilleures performances 
  - **&rarr;**  force brute
  -  **&rarr;** espace trop vaste !

![](https://static.bookstack.cn/projects/hands_on_Ml_with_Sklearn_and_TF/images/chapter_16/16-3.png){height=250px}

## Entrainement (2/3)  <!-- omit in toc -->

- **Solution 2** : utiliser des algorithmes **génétiques**
  - créer aléatoirement une 1ère génération de 100 politiques, les essayer, puis « tuer » les 80 plus mauvaises politiques
  - Un descendant = une copie de son parent avec une variation aléatoire
  - Politiques survivantes et leurs descendants forment la 2ème génération
  - Poursuivre ainsi cette itération sur générations **&rarr;**  évolutions produisent une politique appropriée

![](https://upload.wikimedia.org/wikipedia/commons/4/49/Sexlinked_inheritance_white.jpg){height=250px}

## Entrainement (3/3)  <!-- omit in toc -->

- **Solution 3** : Autres techniques d’optimisation
  - Evaluer les gradients des récompenses
  - Ajuster ces paramètres en suivant le gradient vers des récompenses plus élevées
  - = **gradient de politique** : augmenter légèrement p et évaluer si cela augmente la quantité de poussière ramassée par le robot en 30 minutes
       -  Si oui : p &nearr;
       -  sinon p &searr;

![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Gradient_ascent_%28surface%29.png/585px-Gradient_ascent_%28surface%29.png){height=250px}

## Introduction à OPENAI GYM  <!-- omit in toc -->

- Besoin d'un **environnement simulé**, au moins pour initier l’entraînement
- **OpenAI Gym **(https://gym.openai.com/) = boîte à outils qui fournit divers environnements simulés (jeux Atari, jeux de plateau, simulations physiques en 2D et 3D, etc.)
- Permet d’entraîner des agents

<video height=250px loop autoplay>
    <source type="video/mp4" src="https://gym.openai.com/videos/2019-10-21--mqt8Qj1mwo/SpaceInvaders-v0/original.mp4">
</video>

## Exercice
- Installer openAI Gym :
```
pip3 install --upgrade gym
```
- Exercice du baton

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)


## Exemple avec baton <!-- omit in toc -->

- **Politique** : déclenche une accélération vers la gauche lorsque le bâton penche vers la gauche, et vice versa.
- exécutons cette politique pour voir quelle récompense moyenne elle permet d’obtenir après 500 épisodes
- **&rarr;** au bout de 500 essais, cette politique n’a pas réussi à garder le bâton vertical pendant plus de 68 étapes consécutives
- **&rarr;** on constate que le chariot oscille à gauche et à droite de plus en plus fortement jusqu’à ce que le bâton soit trop incliné
  
## Politique par réseau de neurones

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Observation <!-- omit in toc -->

- réseau de neurones prend une observation en entrée et produira en sortie l’action à exécuter
  - estimera une probabilité pour chaque action
  - sélectionnera aléatoirement une action en fonction des probabilités estimées
  - 2 actions possibles (gauche ou droite) &rarr;  1 neurone de sortie
  - Produira la probabilité p de l’action 0 (gauche) et celle de l’action 1 (droite) sera donc 1 –p

![](https://pic1.zhimg.com/80/v2-b64dc38c5556592e506f84a9cf834e7c_hd.jpg){height=250px}

## Exercice  <!-- omit in toc -->
- Implémentation de la politique

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## Choix pas guidé par le pourcentage <!-- omit in toc -->

- **Pourquoi** sélectionner aléatoirement une action en fonction de la probabilité donnée par le réseau de neurones plutôt que prendre celle qui possède le score le plus élevé ? 
- **&rarr;** Cette approche permet à l’agent de trouver le bon équilibre entre explorer de nouvelles actions et exploiter les actions réputées bien fonctionner
- Exemple : restaurant
  
![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvLh4S8FIXaodRYn9vkPCR1bTWhG_-DvFnLzOlV8EATtgj9FDt&s){height=250px}

## Importance de l'historique <!-- omit in toc -->

- S’il existait un état caché **&rarr;**  prendre en compte les observations et les actions antérieures
- Exemple 1 : environnement avec position du chariot sans vitesse **&rarr;** besoin observation précédente
- Exemple 2 : observations sont bruitées **&rarr;**  tenir compte de quelques observations antérieures
 
![](https://www.lemonde.fr/blog/petrole/files/2019/01/Nomura-deglobalisation.jpg){height=250px}


## Evaluation des actions

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## actions <!-- omit in toc -->

-  meilleure action à chaque étape **&rarr;** entraîner le réseau de neurones
-  **&rarr;** minimiser entropie croisée entre probabilité estimée et probabilité visée
- = apprentissage supervisé normal
- apprentissage par renforcement **&larr;** récompenses (rares et différées)
- Exemple : bâton équilibré pendant 100 étapes
  -  Quelles actions parmi les 100 réalisées étaient bonnes ?
  -  lesquelles étaient mauvaises ? 
  -  sait simplement que le bâton est tombé après la dernière action
  -  **&rarr;** problème d’affectation 

![](https://pic1.zhimg.com/v2-d109cefa13da0fa09be8c2fcf3cbf48c_b.gif){height=150px}

## actions <!-- omit in toc -->

- Stratégie = évaluer une action en fonction de somme des récompenses
-  **&rarr;** appliquer un taux de rabais (**discount rate**) r à chaque étape
- Exemple : si aller à droite trois fois de suite
  -  **&rarr;** reçoit en récompense + 10 après la 1ère étape
  -  **&rarr;** reçoit en récompense 0 après la 2ème étape
  -  **&rarr;** reçoit en récompense -50 après la 3ème étape
  -  avec taux de rabais r = 0,8
  - **&rarr;** action obtient un score total de 10 + r × 0 + r2 × (–50) = –22

![](https://pic3.zhimg.com/80/v2-1ef9c499d38b07cbde47b9abc9b80b72_hd.jpg){height=250px}

## Choix du taux de rabais <!-- omit in toc -->

- Si taux de rabais ≈ 0 :
  - récompenses futures compteront peu / récompenses immédiates
- si taux de rabais ≈ 1
  - récompenses arrivant tardivement compteront / récompenses immédiates
- En général, taux de rabais entre à 0,95 ou 0,99
  - 0,95  **&rarr;**  récompenses qui arrivent 13 étapes dans le futur comptent pour environ la moitié des récompenses immédiates (car 0,9513 ≈ 0,5)
  - 0,99  **&rarr;**  récompenses arrivant 69 étapes dans le futur comptent pour moitié autant que les récompenses immédiates

![](https://static8.depositphotos.com/1257959/833/v/950/depositphotos_8337512-stock-illustration-cartoon-of-men-with-discount.jpg){height=150px}

## Gradients de politique

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Algo REINFORCE <!-- omit in toc -->

::: incremental
1. politique du réseau de neurones jouer plusieurs fois au jeu
   - à chaque étape **&rarr;** calcule les gradients qui augmenteraient la probabilité de l’action choisie
   - on ne les applique pas encore
2. Après plusieurs épisodes
   - calcule le score de chaque action
3. Score positif : l’action était bonne **&rarr;** applique gradients calculés précédemment **&rarr;** action davantage de chances d’être choisie dans le futur
4. score négatif : l’action était mauvaise **&rarr;** applique gradients opposés **&rarr;** action un peu moins probable dans le futur
   - La solution consiste simplement à multiplier chaque vecteur de gradients par le score de l’action correspondante
5. Calcule la moyenne de tous les vecteurs de gradients obtenus et on l’utilise pour effectuer une étape de descente de gradient
:::::


## Exercice <!-- omit in toc -->

- Exercice sur les gradients de politique

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## Processus de décision markoviens

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Présentation<!-- omit in toc -->

- Début XXème, Andrey Markov a étudié les processus stochastiques sans mémoire, appelés **chaînes de Markov**
  - Nombre **fixe** d’états
  - Evolue **aléatoirement** d’un état à l’autre
  - Avec une **probabilité** fixée pour passer de l’état **s** à l’état **s'**
  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Markovkate_01.svg/220px-Markovkate_01.svg.png){height=150px}

- 1950 **&rarr;** processus de décision markoviens 
  - &asymp; chaînes de Markov + choix parmi actions avec probabilités
  - Objectif = maximiser les récompenses au fil du temps

## Exemple <!-- omit in toc -->

![](https://wizardforcel.gitbooks.io/scikit-and-tensorflow-workbooks-bjpcjp/pics/markov-decision-process.png)

- valeur d’état optimale =somme de toutes les récompenses avec rabais futures que l’agent peut espérer en moyenne après qu’il a atteint un état s
- **&rarr;** équation d’optimalité de Bellman

## Exercice <!-- omit in toc -->

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## Apprentissage par différence temporelle <!-- omit in toc -->
- En général, l'agent n'a aucune connaissance des probabilités de transition ou des récompenses
- L'**apprentissage par différence temporelle** (TD Learning) &asymp;  itération des valeurs **+** prend en compte manque de connaissances
- L'algorithme suit la moyenne courante des récompenses anticipées et les plus récentes
- l'algorithme **Q-Learning** s'utilise lorsque les probabilités de transition initiale et les récompenses sont inconnues

## Exercice  <!-- omit in toc -->

- Apprentissage par différence temporelle et apprentissage Q
  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## Politique d'exploration <!-- omit in toc -->

- L’apprentissage Q ne fonctionne que si la politique d’exploration fait suffisamment le tour du MDP
- politique purement aléatoire : ok mais long
- politique **ϵ-greedy** avec proba **ϵ**
- **&rarr;** + de temps à explorer les parties intéressantes

## Exercice <!-- omit in toc -->

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## Apprentissage Q par approximation et apprentissage Q profond

- L’apprentissage Q s’adapte mal aux MDP de grande, voire moyenne, taille, avec de nombreux états et actions
- **solution** : trouver une fonction Qθ(s,a) pour obtenir une approximation des valeurs **Q** en utilisant un nombre de paramètres raisonnable θ

##  Ex: Apprendre à jouer avec PacMan <!-- omit in toc -->

![](https://wizardforcel.gitbooks.io/scikit-and-tensorflow-workbooks-bjpcjp/pics/mspacman-before-after.png)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## DQN du PacMan <!-- omit in toc -->

![](https://wizardforcel.gitbooks.io/scikit-and-tensorflow-workbooks-bjpcjp/pics/mspacman-deepq.png)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## Exercices
