---
author: PF Villard
title: Présentation de l'enseignant
---

## Présentation

- Maître de conférences
	- Enseignement à l'IUT de St-Dié
	- Recherche au Loria à Nancy


## Maître de conférences
<style>
.container{
    display: flex;
    align-items: center;

} 
.col{
    flex: 1;
    display: flex;
    align-items: center;
    align-content: center;
    flex-direction:column;
} 
</style>
<div class="container">
<div class="col">
<li> Page <a href="https://fr.wikipedia.org/wiki/Maître_de_conférences_(France)">Wikipedia</a></li>		
<li> Effectuer des travaux de **Recherche**</li>
<li> Publier des articles</li>
<li> Former aux métiers de la recherche</li>
<li> Effectuer des activités administratives</li>
<li> Effectuer des enseignements</li>
</ul>
</div>
<div class="col">
<iframe  class="scaledFrame"  data-src="https://members.loria.fr/PFVillard/"  style='height:400px; width: 600px;'>
</iframe>
</div>
</div> 


## Parcours
- Maître de conférences à l'UL **depuis 2009**
- Professeur affilié au Harvard Biorobotics Lab **depuis 2014**
- Professeur invité à l'Universite de Bangor **en 2012**
- Post-Doc à Imperial College **2007-2009**
- Post-Doc au CIMIT (MIT/Harvard) **2006-2007**
- Thèse en informatique **2002-2006**
- Ecole d'ingénieur avant

<table>
<tr>
<td><div style="width:100px">![alt text](http://www.ixxi.fr/actualites/bourse-de-these-a-l2019universite-claude-bernard-lyon-ucbl-france-3-ans/image___fr____mini) </div></td>
<td><div style="width:100px">![alt text](https://www.epicicons.com/imager/worklarge/3351/Imperial-College.-SQ-1700-pix_4ae6118128962af1996a94e5370e1193.jpg)</div></td>
<td><div style="width:100px">![alt text](https://s3-eu-west-1.amazonaws.com/global-graduates/institutions/250/d648789f717e609236b6421994e8ca8810c23ed4.png)</div></td>
<td><div style="width:100px">![alt text](https://wgs.fas.harvard.edu/files/wgs/files/harvard_shield_wreath.png)</div></td>
<td><div style="width:100px">![alt text](http://www.portail-humanitaire.org/wp-content/sabai/File/files/l_bb4263107cd945eb01616d3ac43b9572.jpg)</div></td>
</tr>
</table>



## Recherches :
- Modélisation de **déformations**
- Traitement d'**images**
- Réalité **virtuelle**
- Réalité **augmentée**

&rarr; Dans le domaine **médical**

<table>
<tr>
<td><div style="height:100px">![alt text](https://members.loria.fr/PFVillard/files/results/hernia1.png) </div></td>
<td><div style="height:100px">![alt text](http://members.loria.fr/PFVillard/files/supervision/img/Samy.jpg) </div></td>
<td><div style="height:100px">![alt text](http://members.loria.fr/PFVillard/files/results/skeleton.jpg) </div></td>
<td><div style="height:100px">![alt text](http://members.loria.fr/PFVillard/files/results/anatomy.jpg) </div></td>
<td><div style="height:100px">![alt text](http://members.loria.fr/PFVillard/files/results/shapeimage_2.png) </div></td>
</tr>
</table> 


## Exemple 1 : Valve cardiaque

<table>
<tr>
<td><div style="height:400px">![alt text](https://team.inria.fr/curative/files/2017/11/P5prolapse.gif) </div></td>
<td><div style="height:400px">![alt text](https://team.inria.fr/curative/files/2011/07/sofa.gif) </div></td>
</tr>
</table>

&rarr; Voir le site web : <a href="https://team.inria.fr/curative"> <img src="https://team.inria.fr/curative/files/2017/06/logo_curative.png" width="39" height="36"   style="background:none; border:none;"></a>


## Exemple 2 : Modélisation de la respiration

<table>
<tr>
<td><div style="height:300px">![alt text](http://members.loria.fr/PFVillard/files/results/model_respi.gif) </div></td>
<td><div style="height:300px">![alt text](http://members.loria.fr/PFVillard/files/results/parameters.jpg) </div></td>
</tr>
</table>

&rarr; Réalité augmentée et réalité virtuelle


## Réalité virtuelle

![](http://members.loria.fr/PFVillard/files/results/shapeimage_3.png)

<div style="height:300px">
<video autoplay loop controls>
<source src="img/ultrasound_respi.mp4" type="video/mp4">
</video>
</div>



## Réalité augmentée
![](img/ribs.jpg)

<div style="height:300px"> ![](img/anim.gif)</div>


## Deep learning

- Co-supervisé avec *Fabien Pierre*
- 2 travaux:
    - Détection des défauts sur les grilles métalliques
    - Segmentation du thorax sur tomodensitométrie

![](img/metalGrid.png){ width=20% } 
![](img/thorax.jpg){ width=20% } 


## Grillage

- Deux types de grillage (100 images/grille)
- 50 images avec défaut/50 sans défaut
- Fond noir ou blanc
  
![](img/gridData.png){ width=50%  } 


## Image médicale   <!-- omit in toc -->

<font size=4>
<div class="container">
Lungs
<div class="col">
Training data
![](img/P1_lungs_resultat_40_SN.png)
</div>
<div class="col">
Test data
![](img/P6_lungs_resultat_40_SN.png)
</div>
</div>

<div class="container">
Diaph
<div class="col">
![](img/P1_diaph.png)
</div>
<div class="col">
![](img/P6_diaph.png)
</div>
</div>
</font>