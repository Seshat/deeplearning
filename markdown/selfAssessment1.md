---
author: PF Villard
title: Deep Learning
subtitle: Auto evaluation 1
progress: true
slideNumber: true
navigationMode: 'linear'
---
# Modèle linéaire <!-- omit in toc --> 

## Exemple 1

Soit un ensemble de données mesurées :

[exemple1](jupyterNotebook/TD1/ex1.html)

- Que valent $n$ et $m$ ?
- Où sont $X$, $\Theta$ et $y$
- Quel est l'**inconnu** pour construire le modèle ?
- A quoi ressemble le modèle **géométriquement** ?

## Suite

<iframe width="600" height="300" src="jupyterNotebook/TD1/ex2.html"></iframe>

- Comment un tel **modèle** peut-il être trouvé ?
- Quelle est la solution la **plus précise et la plus rapide** ?


## Exemple 2

- On a mesuré dans plusieurs académies de football plusieurs métriques à l'aide de **capteurs sur les chaussures** :
  - vitesse, accélération, position
  - Zones de présence
  - Nombre de passes et à qui
  - etc.
- Chacun des $m$ joueurs ont été suivis dans leur carrière pour determiner la **longévité**
- **&rarr;** Comment utiliser un algorithme de regression linéaire pour prédire la longévité d'un nouvel arrivant ?




## Suite

- Où sont $X$, $\Theta$ et $y$ dans ce problème ?
- Quelle est la solution **la plus précise et la plus rapide** pour construire le modèle ?
- Quels sont les caractéristiques qui vont **augmenter la précision** du modèle ?
- Quels seront les hyper-paramètres à régler ?
- Une fois que le modèle est connu, quel est l'**inconnu** pour prédire la longévité d'un nouvel arrivant ?

![](https://upload.wikimedia.org/wikipedia/commons/e/ea/1alessandromartinelli2015.jpg){height=250px}

## Exemple 3

- Soit le jeu de données ci-dessous :
 
<iframe width="600" height="300" src="jupyterNotebook/TD1/ex3.html"></iframe>

- Où sont $X$, $\Theta$ et $y$ ?
- Quel est l'**inconnu** pour construire le modèle ?
- Quelle est la méthode à utiliser ?
- Quels seront les hyper-paramètres à régler ?


## Suite

| <img  height="300" src="jupyterNotebook/TD1/learn1.png"> |  <img height="300" src="jupyterNotebook/TD1/learn2.png"> |  <img height="300" src="jupyterNotebook/TD1/learn3.png"> |
|----------|:-------------:|:-------------:|

- Que dire de ces courbes d'apprentissage ?


## Support Jupyter pour la suite

[ici](jupyterNotebook/TD1/TDassessment.ipynb)