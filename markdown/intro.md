---
author: PF Villard
title: Deep Learning
---
## Plan du cours
- [Les fondamentaux](html/IA1.html)
- [Perceptron multicouche](html/IA2.html)
- [CNN](html/IA3.html)
- [Apprentissage par renforcement](html/IA4.html)

## Présentation de l'enseignant

Une présentation de l'enseignant se trouve [ici](pf.html)

## Références

- Images :
    -  [Wikipedia](https://en.wikipedia.org)
    -  [Machine Learning for Artists](https://ml4a.github.io/)
- D'après :
    - Le cours d'[Andrew Ng](https://en.wikipedia.org/wiki/Andrew_Ng) sur [Coursera](https://www.coursera.org/learn/machine-learning)
    - [Machine Learning for Artists](https://ml4a.github.io/)
    - [Deep Learning avec TensorFlow](https://ulysse.univ-lorraine.fr/permalink/33UDL_INST/1u932an/alma991010388389705596), Aurélien Géron, Dunod - 2017
    - l'article de Jason Brownlee sur [Deep Learning Performance](https://machinelearningmastery.com/learning-curves-for-diagnosing-machine-learning-model-performance/)

## Autres supports
- PDF :
    - [Les fondamentaux](pdf/IA1.pdf)
    - [Perceptron multicouche](pdf/IA2.pdf)
    - [CNN](pdf/IA3.pdf)
    - [Apprentissage par renforcement](pdf/IA4.pdf)
- HTML :
    - [Les fondamentaux](docHtml/IA1.html)
    - [Perceptron multicouche](docHtml/IA2.html)
    - [CNN](docHtml/IA3.html)
    - [Apprentissage par renforcement](docHtml/IA4.html)


## Format du cours

- Cours en **HTML5**, **CSS3**
- Rédigé en [Markdown](https://daringfireball.net/projects/markdown/) et compilé par [pandoc](https://pandoc.org)
- fonctionnalités supplémentaires :
    - [reveal.js](https://github.com/hakimel/reveal.js/) **&rarr;** présentation dynamique
    - [chart.js](https://www.chartjs.org) **&rarr;** courbes
    - [MathJax](https://www.mathjax.org) **&rarr;** équations mathématiques
    - Exercices disponibles avec les fichiers **jupyter notebook** fournis 
<!-- pandoc -t revealjs -s -o index.html intro.md -V revealjs-url=./reveal.js  -V transition=cube  -f markdown+smart -V theme=beige --mathjax --css slide.css -->

## Introduction

- Intelligence artificielle, apprentissages automatique et profond

![](https://ecoinfo.cnrs.fr/wp-content/uploads/2019/10/ia_ml_dl.png)
<!-- https://ecoinfo.cnrs.fr/2019/10/01/impact-environnemental-de-lia/ -->

## Introduction

- Processus d’apprentissage profond supervisé
  
![](img/archi_appr.jpg)


## Objectif du cours

- Comprendre les **bases** de
    - La regression linéaire
    - L'apprentissage à base de **réseaux de neurones**
    - L'apprentissage à base de réseaux de neurones profonds
- Etre capable d'utiliser un **ToolKit** d'apprentissage 
- Etre capable d'**évaluer la pertinence** d'un résultat donné par un toolkit

<div style="height:300px">![](https://i.morioh.com/b4d8a3dfdf.png)</div>


## Problématique de la validation

- Si tout est codé *normalement*, un ToolKit donne **toujours** un résultat
    -  **&rarr;** Il ne suffit pas d'avoir un code qui **compile** et qui s'**exécute**, il faut toujours **analyser** le résultat !
 -  **&rarr;** Savoir détecter des problèmes
 -  **&rarr;** Savoir tester l'efficacité d'un entraînement
 -  Eviter les problèmes comme dans cet [Exemple](https://www.sciencesetavenir.fr/high-tech/intelligence-artificielle/intelligence-artificielle-la-reconnaissance-faciale-est-elle-misogyne-et-raciste_121801) (autre [article](http://proceedings.mlr.press/v81/buolamwini18a/buolamwini18a.pdf) en anglais)

<div style="height:300px">![](https://www.sciencesetavenir.fr/assets/img/2018/03/06/cover-r4x3w1000-5a9e76d1e35f5-facerecog-iphone.jpg)</div>

## Application concrète

- A partir de modèles [imprimés en 3D](https://gitlab.inria.fr/Seshat/deepAsterix/)
- **Acquisition** d'une base de donnée
- Conception de l'**architecture** 
- **Apprentissage**
- Mise en production dans une [appli web](https://members.loria.fr/PFVillard/files/asterix/essai.html)

![](https://gitlab.inria.fr/Seshat/deepAsterix/-/raw/master/paint/im12.jpg){height=300px}


## Notation

- 1 note donnée par Fabien Pierre
- 1 note lors du **TP noté le 12 janvier**

## Fonctionnement du cours

- Alternance **cours** / Jupyter **Notebook**
- Arborescence:
     - TD1
     - TD2
     - TD3
     - TP
- Attention pour les Jupyter Notebook: 
     - Fichiers sur Arche à mettre dans ```TP```
     - Ordre d'Exécution:
          - ```X=[0, 2, 0, 15]```  
          - ```print(X)```
          - ```X=[5, 8, 9]```
   
   


## Installation

Pour l'installation de l'environnement, consulter ce [site](anaconda.html).
