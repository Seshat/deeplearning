---
author: PF Villard
title: Deep Learning
subtitle: Les fondamentaux
progress: true
slideNumber: true
navigationMode: 'linear'
---
<!-- pandoc -t revealjs -s -o IA1.html IA1.md -V revealjs-url=./reveal.js  -V transition=cube  -f markdown+smart -V theme=beige --mathjax --css slide.css  --include-after-body='debut.txt'-->

- [Introduction](#introduction)
- [Régression linéaire](#régression-linéaire)
- [Descente de gradient](#descente-de-gradient)
- [Exemple d'utilisation pratique](#exemple-dutilisation-pratique)
- [Descente de gradient stochastique](#descente-de-gradient-stochastique)
- [Courbes d’apprentissage](#courbes-dapprentissage)
- [Modèle linéaire régularisé](#modèle-linéaire-régularisé)
- [Régression logistique](#régression-logistique)
- [Exemple](#exemple)
- [Régression softmax](#régression-softmax)
- [Exercices](#exercices)


## Introduction

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Principe Général<!-- omit in toc -->

- Créer un modèle prédictif
- Régler les paramètres (avec données d’entraînement)
- Utiliser ce modèle pour faire des prédictions sur de nouvelles observations

<br/><br/>
<div class="container"  style="width: 100%;margin: 0 auto;">
<div class="col"  style="width: 100%;margin: 0 auto;">
<canvas id="chart2c"></canvas>
</div>
<div class="col"  style="width: 100%;margin: 0 auto;font-size:15px"> 
- Modèle : **y=ax+b**
- Paramètres : **a** et **b**
- Observations : ($x_1,y_1$),  ($x_2,y_2$), ... 
</div>
</div>

## Si performance moindre<!-- omit in toc --> 

- = modèle a « surajusté » le jeu de données d’entraînement
- = trop de paramètres / quantité de données d’entraînement
- Solution 1 : réentraîner sur plus gros jeu
- Solution 2 : choisir un modèle plus simple
- Solution 3 : contraindre le modèle (régularisation)

<div style="width: 70%;margin: 0 auto;">
<canvas id="chart12"></canvas>
</div>

## Si modèle mauvais sur données d’entraînement<!-- omit in toc --> 

- = « sous-ajuste » les données d’entraînement
- Solution 1 : utiliser un modèle plus puissant 
- Solution 2 : diminuer le degré de régularisation

<div style="width: 70%;margin: 0 auto;">
<canvas id="chart24"></canvas>
</div>

## Exemple de ce principe<!-- omit in toc --> 

- Prédire le prix d’une maison en fonction de sa superficie et du revenu des habitants du quartier

<div style="width: 70%;margin: 0 auto;">
<canvas id="chart1"></canvas>
</div>



##  modèle linéaire <!-- omit in toc --> 
  - &rarr; somme pondérée des paramètres + terme constant 
$$
prix=\theta_0+\theta_1 \times superficie
$$

<canvas id="chart2"></canvas>

- &rarr; prédire le prix de nouvelles maisons


## Exercices TD<!-- omit in toc --> 

Comprendre un modèle linéaire en 2D : [JupyterNotebook](../jupyterNotebook/TD1/TD1.ipynb)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)


## Régression linéaire

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Définition<!-- omit in toc -->

= somme pondérée des variables d’entrée + un terme constant
$$
\hat{y}=\theta_0+\theta_1 x_1 + \theta_2 x_2+...+\theta_n x_n
$$
avec :

- $\hat{y}$ la valeur prédite
- $n$ le nombre de variables
- $x_i$ la valeur de la $n^{ème}$ variable
- $\theta_j$ le $j^{ème}$ paramètre du modèle

## Système<!-- omit in toc --> 

On réalise plusieurs observations $x^{(1)}$, $x^{(2)}$,..., $x^{(m)}$

Cela permet de résoudre le système suivant :

$$
\begin{cases}
\hat{y}^{(1)}=\theta_0+\theta_1 x_1^{(1)} + \theta_2 x_2^{(1)}+...+\theta_n x_n^{(1)}\\
\hat{y}^{(2)}=\theta_0+\theta_1 x_1^{(2)} + \theta_2 x_2^{(2)}+...+\theta_n x_n^{(1)}\\
...\\
\hat{y}^{(m)}=\theta_0+\theta_1 x_1^{(m)} + \theta_2 x_2^{(m)}+...+\theta_n x_n^{(m)}\\
\end{cases}
$$


## Notation vectorielle <!-- omit in toc --> 

$$
\begin{cases}
\hat{y}^{(1)}=\theta_0+\theta_1 x_1^{(1)} + \theta_2 x_2^{(1)}+...+\theta_n x_n^{(1)}\\
\hat{y}^{(2)}=\theta_0+\theta_1 x_1^{(2)} + \theta_2 x_2^{(2)}+...+\theta_n x_n^{(2)}\\
...\\
\hat{y}^{(m)}=\theta_0+\theta_1 x_1^{(m)} + \theta_2 x_2^{(m)}+...+\theta_n x_n^{(m)}\\
\end{cases}
$$

devient sous forme vectorielle :
$$
\begin{gather}
\begin{bmatrix} 
\hat{y}^{(1)}\\
\hat{y}^{(2)}\\
...\\
\hat{y}^{(m)}
\end{bmatrix}=
\begin{bmatrix} 
1 &   x_1^{(1)} &  x_2^{(1)} &  ... &  x_n^{(1)}\\
1 &   x_1^{(2)}  & x_2^{(2)} &  ...  & x_n^{(2)}\\
...\\
1 &   x_1^{(m)}  & x_2^{(m)}  & ...  & x_n^{(m)}
\end{bmatrix}
\begin{bmatrix} 
\theta_0  \\
\theta_1 \\
\theta_2 \\
...\\
\theta_n
\end{bmatrix}
\end{gather}
$$ 

Soit : $\boldsymbol{\hat{\boldsymbol{y}}}= \boldsymbol{X} \boldsymbol{\theta}$


## Notation  vectorielle <!-- omit in toc -->

- Vecteurs représenté en gras (ex: $\boldsymbol{\theta}$)
- Par défaut, les vecteur sont des vecteurs *colonne*
- &rarr; tableaux à **2 dimensions**
- $\boldsymbol{X} \boldsymbol{\theta}$ est un produit matriciel
- \boldsymbol{X} est le vecteur des valeurs des observation avec $x_0=1$


## Exercices TD<!-- omit in toc --> 

Comprendre la notation  vectorielle : suite du  [JupyterNotebook](../jupyterNotebook/TD1/TD1.ipynb)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)


## Mesure de la précision du modèle<!-- omit in toc -->

- = mesure de l'erreur commise
- => Fonction de coût
- Le plus courant : RMSE

**racine carrée de l’erreur quadratique moyenne**

$$
RMSE(X,h)=\sqrt{\frac{1}{m}\sum_{i=1}^m \left[ h(x^{(i)})-y^{(i))} \right]^2}
$$

&rarr; Trouver les paramètres $\theta_j$  qui minimisent RMSE
&rarr; En pratique : **MSE**

![](https://seshat.gitlabpages.inria.fr/deeplearning/img/rmse.png){height=250px}




## Exercices TD<!-- omit in toc --> 

Comprendre la mesure du coût : suite du  [JupyterNotebook](../jupyterNotebook/TD1/TD1.ipynb)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)


## Équation normale<!-- omit in toc --> 

- Equation qui permet de trouver les paramètres $\theta_j$ :
  
$\theta_j=(X^T.X)^{-1}.X^T.y$

<canvas id="chart2b"></canvas>


## Exercice<!-- omit in toc --> 

*Comment anticiper le nombre d'heures à jouer à un jeu vidéo pour atteindre un nombre minimum de point ?*

**&rarr; TP !**

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## Bilan équation normale<!-- omit in toc --> 

- Désavantage: complexité algorithmique assez élevée
	- si nb variables $\times 2$, temps de calcul $\times 8$
- Avantage: linéaire par rapport au nb d'observations* (si tient en mémoire)
	- si nb *observation* $\times 2$, temps de calcul $\times 2$
- &rarr; Ne pas utiliser si beaucoup de variables
- &rarr; Ne pas utiliser si trop d’observations pour tenir en mémoire


## Exercices TD<!-- omit in toc --> 

Comprendre la contrainte de temps dans l'équation normale : suite du  [JupyterNotebook](../jupyterNotebook/TD1/TD1.ipynb)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)


## Descente de gradient

- Algorithme d’optimisation très général
- Idée : corriger petit à petit les paramètres pour minimiser une fonction de coût
- Algo :
	1. remplir $\theta$ avec valeurs aléatoires
	2. améliorer progressivement pour que RMSE &searr;
	3. jusqu'à convergence vers minimum

![](https://miro.medium.com/max/1005/1*_6TVU8yGpXNYDkkpOfnJ6Q.png){height=300px}

## Problème <!-- omit in toc --> 

<div class="container">
<div class="col">
![](https://www.charlesbordet.com/assets/images/gradient-descent/gradientdescent-alpha0.05.gif)
</div>
<div class="col">
![](https://www.charlesbordet.com/assets/images/gradient-descent/gradientdescent-f3.gif)
</div>
</div>

## Influence du taux d'apprentissage <!-- omit in toc --> 

- **Hyperparamètre** de l'apprentissage
- &rarr; pas optimisé 
- Si taux d'apprentissage petit
- &rarr; grans nombre d'itérations
- Si taux d'apprentissage grand
- &rarr; divergence de l'algo

<div  style="width: 75%;margin: 0 auto;">
<button id="gd1">**$\eta$**=0.02</button>
<button id="gd2">**$\eta$**=0.1</button>
<button id="gd3">**$\eta$**=0.5</button>
<canvas id="chart4"></canvas>
</div>


## Exemple d'utilisation pratique

![](https://miro.medium.com/max/1400/1*70f9PB-RwFaakqD6lfp4iw.png)

## Influence de la fonction de coût <!-- omit in toc --> 

- Minimum **local** différent d'un minimum **global**
- Pas le cas de la MSE, du modèle linéaire
- &rarr; Fonction **convexe**

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/ConvexFunction.svg/1280px-ConvexFunction.svg.png)

##   Normalisation de paramètres <!-- omit in toc --> 
- Fonction de coût en forme de bol déformé avec 2 paramètres
- &rarr; Influence sur la convergence
- &rarr; Il faut redresser le bol avec la **Normalisation** 
- &rarr; A la fin de l'apprentissage, il faut appliquer la transformation inverse

![](https://miro.medium.com/max/1400/1*vXpodxSx-nslMSpOELhovg.png)

##   Implémenter une descente de gradient <!-- omit in toc --> 

- Il faut formuler la modification de la fonction de coût lorsque $\theta_j$ est un peu modifié
- = **Dérivée partielle**
- Formule :
$$
\frac{\partial }{\partial \theta_i}MSE(\theta)=\frac{2}{m}\sum_{i=1}^m(\theta^t.x^{(i)}-y^{(i)})x_j^{(i)}
$$
- &rarr; à calculer pour tous les paramètres
- &rarr; Calcul sur l'ensemble du jeu de données
- &rarr; **batch gradient descend** = groupé
- &rarr; **lent** avec beaucoup de données
- &rarr; **rapide** avec beaucoup de paramètres

## Utilisation <!-- omit in toc --> 

- Soustraire le gradient à la précédente valeur de $\theta$
- Utilisation du taux d'apprentissage
$$
\theta \leftarrow \zeta \nabla_{\theta}MSE(\theta)
$$
- Convergence :
1. Choisir un très grand nombre d'itérations
2. Interrompre lorsque le gradient est très petit
- &rarr; lorsque sa norme est inférieure à $\epsilon$
- = **tolérance**

## Exercice<!-- omit in toc --> 

*Comment anticiper le nombre d'heures à jouer à un jeu vidéo pour atteindre un nombre minimum de point en utilisant une descente de gradient classique ?*

**&rarr; TP !**

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)


## Descente de gradient stochastique
- Idée: ne pas utiliser tout le temps l'ensemble du jeu de données
- N'utiliser qu'une observation à chaque itération
- &rarr; Moins régulier 
- &rarr; Arrive au minimum sans converger
- &rarr; Résultats bons mais pas optimums
- &rarr; Peut permettre d'échapper aux minimums locaux
- &rarr; Possibilité de réduire le taux d'apprentissage
  
## Epoque <!-- omit in toc --> 
- Un cycle de **m** itérations
- m= nombre d'observations dans le jeu de données d'entraînement
- chacun de ces cycles est appelé une **époque**

<canvas id="chart6"></canvas>


## Exercices TD<!-- omit in toc --> 

Comprendre le gradient stochastique : suite du  [JupyterNotebook](../jupyterNotebook/TD1/TD1.ipynb)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)


## Exercice<!-- omit in toc --> 

*Comment anticiper le nombre d'heures à jouer à un jeu vidéo pour atteindre un nombre gapde point en utilisant une descente de gradient stochastique ?*

**&rarr; TP !**

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## Descente de gradient par mini-lots<!-- omit in toc -->
- = **mini-batch gradient descend**
- A mi-chemin entre le DG ordinaire et le DG stochastique
- &rarr; calcul sur de petits sous-ensembles d'observation
- &rarr; Rapide avec le GPU !
- Peut avoir du mal à sortir d'un minimum local

<canvas id="chart7"></canvas>

## Comparaison<!-- omit in toc -->

| Algorithme           | m  grand | Hors-mémoire possible ? | n grand | Hyper-paramètres | Normalisation requise ? |
| -------------------- | :------: | :---------------------: | :-----: | :--------------: | :---------------------: |
| **Équation normale** |  rapide  |           non           |  lent   |        0         |           non           |
| **DG ordinaire**     |   lent   |           non           | rapide  |        2         |           oui           |
| **DG stochastique**  |  rapide  |           oui           | rapide  |       ≥ 2        |           oui           |
| **DG par mini-lots** |  rapide  |           oui           | rapide  |       ≥ 2        |           oui           |

## Régression polynomiale<!-- omit in toc -->
- A utiliser quand pas modélisable par une droite
- &rarr; Ajouter les **puissances** de chacune des variables
- &rarr; Entrainer un **modèle linéaire**

<div>
<button id="deg2">**deg**=2</button>
<button id="deg5">**deg**=5</button>
<button id="deg10">**deg**=10</button>
<button id="deg25">**deg**=25</button>
<button id="deg75">**deg**=75</button>
</div>

<canvas id="chart8"></canvas>


## Exercices TD<!-- omit in toc --> 

Comprendre la régression polynomiale : suite du [JupyterNotebook](../jupyterNotebook/TD1/TD1.ipynb)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)


## Exercice<!-- omit in toc --> 

Comment anticiper le nombre de vues d'une vidéo YouTube en fonction du temps écoulé depuis qu'elle a été mise en ligne ?

**&rarr; TP !**

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)

## Courbes d’apprentissage

<img src="../logo.png" style="background:none; border:none; box-shadow:none;width: 30%;margin: 0 auto;-webkit-box-reflect: below 50px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(20%, transparent) , to(rgba(250, 250, 250, 0.6)));">

## Introduction<!-- omit in toc -->

- Courbe pour modéliser les performances  
- &rarr; Permet de détecter des problèmes lors de l'apprentissage
  - Sous-apprentissage
  - Sur-apprentissage
  - Problèmes avec le volume des données

<canvas id="chart13"></canvas>

## Définition<!-- omit in toc --> 

Courbe avec :

- le temps ou l'expérience en X
-  l'apprentissage ou l'amélioration en Y

Ex : compétence pour jouer de la guitare

<div  style="width: 75%;margin: 0 auto;">
<canvas id="chart14"></canvas>
</div>

<!-- ## Definition en apprentissage profond

- Quantifier la qualité de l'apprentissage
- ex: précision d'une classification
  - &rarr; doit être maximisé -->

## En pratique<!-- omit in toc --> 

- Minimisation de l'erreur
- A chaque étape de calcul, l'algo d'entrainement doit être évalué
- Il y a 2 types de courbes :
  - Courbe d'apprentissage sur l'entrainement
  - Courbe d'apprentissage sur la validation
- &rarr; commun d'étudier les 2
- 3 types de comportements
  - Sur-apprentissage
  - Sous-apprentissage
  - Bon apprentissage

![](https://www.publicdomainpictures.net/pictures/290000/nahled/online-doctor.jpg){width=50%}

<!-- ## Métriques 
- Commun de calculer plusieurs métriques.
- Exemple en classification :
  - Entropie croisée &rarr; précisison
  - Précision de la classification &rarr; performance
- &rarr; 2 courbes -->



## Sous apprentissage<!-- omit in toc --> 

- Ne peut pas apprendre avec les données d'entraînement
  - Ligne plate 
  - Données bruitées avec valeurs hautes
  - Décroit jusu'à la fin de l'entraînement
  
<div style="width: 75%; margin: 0 auto">
<canvas id="chart15"></canvas>
</div>  

## Sur-apprentissage<!-- omit in toc --> 

- Apprend trop y compris le bruit
- &rarr; **+** un modèle est précis **-** il est générique
  - Ligne continue qui décroit
  - Courbe de validation avec erreur &nearr;

<div  style="width: 75%; margin: 0 auto">
<canvas id="chart16"></canvas>
</div>

## Bon apprentissage<!-- omit in toc --> 

- Stable, bon compromis
- Ecart minimal entre les courbes
- Appelé "*generalization gap*"

<div  style="width: 75%; margin: 0 auto">
<canvas id="chart14b"></canvas>
</div>

## Diagnostiquer des données pas représentatives<!-- omit in toc --> 

- &rarr; Entrainement
- &rarr; Validation

- Pas assez de données d'entrainement
  - pas assez de données pour apprendre un modèle générique
  - Amélioration sur l'entraînement
  - Amélioration sur l'amélioration
    - mais pas un gros gap
- Pas assez de données de validation
  - Validation avec bruit
    - ou bien en dessous
    - &rarr; plus facile à prédire

 Comprendre les courbes d'apprentissage

## Modèle linéaire régularisé

- réduire le surajustement **&larr;** régulariser le modèle
- **&larr;** si degrés de liberté **&searr;**, surajustement **&searr;**
- ex : régulariser un modèle polynomial = **&searr;** le nombre de degrés du polynôme
- &rarr; imposer des contraintes aux coefficients de pondération du modèle
  
<div class="container"  style="width: 100%;margin: 0 auto;">
<div class="col"  style="width: 100%;margin: 0 auto;"> 
<canvas id="chart20b"></canvas>
</div>
<div class="col"  style="width: 100%;margin: 0 auto;"> 
<canvas id="chart21"></canvas>
</div>
</div>

## Régression ridge<!-- omit in toc -->

- **=** *régression de crête* ou *régularisation de Tikhonov*
- version régularisée de la régression linéaire
- un terme de régularisation est ajouté à la fonction de coût:
$$
\alpha \sum_{-1}^n\theta_i^2
$$
- ajouté à la fonction de coût que durant l’entraînement
- L’hyperparamètre **$\alpha$** contrôle la quantité de régularisation
  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Tsubakurodake_from_Otenshodake_2002-8-22.jpg/800px-Tsubakurodake_from_Otenshodake_2002-8-22.jpg){height=300px}

## Régression lasso<!-- omit in toc --> 
- **=**  *Least Absolute Shrinkage and Selection Operator*
- un terme de régularisation est ajouté à la fonction de coût:
$$
\alpha \sum_{1}^n|\theta_i|
$$
- tend à éliminer complètement les poids des variables les - importantes
  
![](https://upload.wikimedia.org/wikipedia/commons/6/6f/2008_Kentucky_State_Fair_Roping_Show_%282765926132%29.jpg){height=300px}

## Exemple en régression linéaire <!-- omit in toc -->

<div style="width: 100%;margin: 0 auto;">
<button id="alpha000">**$\alpha$**=0</button>
<button id="alpha010">**$\alpha$**=10</button>
<button id="alpha100">**$\alpha$**=100</button>
</div>

<canvas id="chart17"></canvas>

## Exemple en régression Polynomiale <!-- omit in toc -->

<div style="width: 100%;margin: 0 auto;">
<button id="alpha00">**$\alpha$**=0</button>
<button id="alpha05">**$\alpha$**=$1e^{-5}$</button>
<button id="alpha01">**$\alpha$**=100</button>
</div>

<canvas id="chart20"></canvas>


## Exercice<!-- omit in toc --> 


Comment anticiper le nombre de vues d'une vidéo YouTube en fonction du temps écoulé depuis qu'elle a été mise en ligne : influence de la courbe d'apprentissage.

**&rarr; TP !**

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)
  

## Régression logistique 

- Utilisation de la régression pour la **classification**
- = estimer la probabilité qu’une observation appartienne à une classe particulière
  -  Si proba > 50%, modèle prédit que l’observation appartient à cette classe
     - classe positive, d’étiquette « 1 »
  -  sinon il prédit qu’elle appartient à l’autre classe
     - classe négative, d’étiquette « 0 »)
- **&rarr;** classificateur binaire

## fonction logistique<!-- omit in toc --> 
- = fonction sigmoïde 
- &asymp; régression linéaire : 
  - calcule une somme pondérée des caractéristiques d’entrée (plus un terme constant)
  - **au lieu** de fournir le résultat directement, il fournit la **logistique** du résultat
  
 $\sigma(t) = \frac{1}{1 + e^{-t}}$

<div style="width: 60%;margin: 0 auto;">
<canvas id="chart23"></canvas>
</div>

## Exemple

<div style="width: 60%;margin: 0 auto;">
<canvas id="chart26"></canvas>
</div>


## Régression softmax

- Le modèle de régression logistique peut être généralisé de manière à prendre en compte **plusieurs classes** directement
- **principe** :  étant donné une observation **$x$**, le modèle de régression *softmax* calcule d’abord un score **$s_k(x)$** pour chaque classe **$k$**, puis estime la probabilité de chaque classe en appliquant aux scores la fonction *softmax* 
$$
s_k(x)=(\theta^{(k)})^\intercal.x
$$

## Exercices

A partir de cookies de site web, comment anticiper vers quels nouveaux utilisateurs  envoyer de la publicité ciblée ?

**&rarr; TP !**

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/250px-Jupyter_logo.svg.png)