#!/bin/sh

export myPandocSlide="pandoc -t revealjs -s  -V transition=cube   -f markdown -V theme="league" --mathjax    --css slide.css  -V revealjs-url=./reveal.js --template=myTemplate.html"

export myPandocHTML="pandoc  -s -f markdown  --mathjax --css doc.css"

export myPandocPDF="pandoc -t latex  -s -f markdown -V --mathjax --latex-engine=xelatex"

ls index.html
$myPandocSlide -o index.html markdown/intro.md 
ls index.html

$myPandocHTML -o docHtml/index.html markdown/introDoc.md

$myPandocSlide -o anaconda.html markdown/anaconda.md
$myPandocSlide -o selfAssessment1.html markdown/selfAssessment1.md
$myPandocSlide -o selfAssessment2.html markdown/selfAssessment2.md
$myPandocSlide -o pf.html markdown/pf.md

mkdir pdf
cp myTemplate.html html
cp -r common html
cp -r javascript html
cp -r chart reveal.js/plugin/
cp -r reveal.js html
cp slide.css html



#for ((i = 1; i < 5; i++)); do
i=1
while [ "$i" -le 4 ]; do
    echo '<script src="../javascript/scriptAI'$i'.js"></script>' > script.txt
    cat html/begin.html script.txt > otherStuff.txt  
    $myPandocSlide -o html/IA$i.html markdown/IA$i.md  --include-after-body=otherStuff.txt
    cat docHtml/beginDoc.html script.txt > otherStuff.txt  
    $myPandocHTML -o docHtml/IA$i.html markdown/IA$i.md  --include-after-body=otherStuff.txt 
    $myPandocPDF -o pdf/IA$i.pdf markdown/IA$i.md 
    echo "IA$i done"
    i=$(( i + 1 ))
done
#rm *.txt