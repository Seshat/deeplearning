# To support both python 2 and python 3
from __future__ import division, print_function, unicode_literals

import numpy as np
import os
import drawBasics as db

np.random.seed(42)
n_epochs = 50
t0, t1 = 5, 50  # learning schedule hyperparameters

def plot_miniBash_gradient(f,X,y,theta, X_b,X_new,X_new_b, theta_path=None):
    n_iterations = 50
    minibatch_size = 20
    m = len(X_b)
    theta_path_mgd = []
    theta = np.random.randn(2,1)  # random initialization

    t0, t1 = 200, 1000
    def learning_schedule(t):
        return t0 / (t + t1)

    t = 0
    for epoch in range(n_iterations):
        shuffled_indices = np.random.permutation(m)
        X_b_shuffled = X_b[shuffled_indices]
        y_shuffled = y[shuffled_indices]
        for i in range(0, m, minibatch_size):
            y_predict = X_new_b.dot(theta)
            db.drawLine(f,X_new[0],y_predict[0],X_new[1],y_predict[1])
            t += 1
            xi = X_b_shuffled[i:i+minibatch_size]
            yi = y_shuffled[i:i+minibatch_size]
            gradients = 2/minibatch_size * xi.T.dot(xi.dot(theta) - yi)
            eta = learning_schedule(t)
            theta = theta - eta * gradients
            theta_path_mgd.append(theta)
    return theta_path_mgd