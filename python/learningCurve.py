
import numpy as np
import os
import drawBasics as db

from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

def plot_learning_curves(f, model, X, y, testSize):
    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=testSize, random_state=10)
    train_errors, val_errors = [], []
    for m in range(1, len(X_train)):
        model.fit(X_train[:m], y_train[:m])
        y_train_predict = model.predict(X_train[:m])
        y_val_predict = model.predict(X_val)
        train_errors.append(mean_squared_error(y_train[:m], y_train_predict))
        val_errors.append(mean_squared_error(y_val, y_val_predict))
    f.write('           datasets: [{},{\n')
    f.write('           label: "Jeu d\'entrainement",\n')
    nb=np.arange(1,len(train_errors))
    db.drawPolyLine(f,nb,np.sqrt(train_errors),'"#ff6384"')
    f.write('              ,\n')
    f.write('              {\n')
    f.write('           label: "Jeu de validation",\n')
    db.drawPolyLine(f,nb,np.sqrt(val_errors),'"#36a2eb"')