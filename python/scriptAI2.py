############################################
#                                          #
#      Python script to create             #
#        figure with chart.js              #
#                                          #
############################################

# INPUT:
#########
# nothing

# OUTPUT:
#########
# chart....txt -> single chart
# final.html    -> Concatenation of the above 

import numpy as np
import sys
import glob
import os
import drawBasics as db
import gradientDescent as gd
import stochasticGradient as sg
import miniBashGradient as mbg
import polynomial
import learningCurve as lc
import regularization as regul

from sklearn.linear_model import Ridge
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures


def sigmoid(z):
    return 1 / (1 + np.exp(-z))

def relu(z):
    return np.maximum(0, z)

def derivative(f, z, eps=0.000001):
    return (f(z + eps) - f(z - eps))/(2 * eps)

t = np.linspace(-10, 10, 100)

#######################################
# Activation function
#######################################
## Chart 1
chartNumber=1
f = open("charts1.txt", "w")
name='activ'
db.header(f,name)
f.write('           datasets: [{\n')
f.write('           label: "Step",\n')
db.drawPolyLine(f,t,np.sign(t),'window.chartColors.red')
f.write('              ,\n')
f.write('              {\n')
f.write('           label: "Sigmoid",\n')
db.drawPolyLine(f,t, np.tanh(t),'window.chartColors.blue')
f.write('              ,\n')
f.write('              {\n')
f.write('           label: "Tanh",\n')
db.drawPolyLine(f,t, relu(t),'window.chartColors.green')
f.write('              ,\n')
f.write('              {\n')
f.write('           label: "ReLU",\n')
db.drawPolyLine(f,t, sigmoid(t),'window.chartColors.purple')
db.footer(f) 
db.defineChart(f,name,'chart25','true',-5,5,-1,1)
f.close() 

# Prepare the html file
# ##########################################
#Concatenate the variable and the scene files
print(chartNumber)
# os.system("cat header.html charts1.txt > temp1.txt")
os.system("cat charts1.txt > temp1.txt")
for cpt in range(1, chartNumber):
    cmd="cat temp"+str(cpt)+".txt charts"+str(cpt+1)+".txt  > temp"+str(cpt+1)+".txt"
    os.system(cmd)
    print(cmd)
cmd="cp temp"+str(chartNumber)+".txt ../javascript/scriptAI2.js"  
os.system(cmd)
# cmd="cat temp"+str(chartNumber)+".txt footer.html  > final.html"    
# os.system(cmd)
print(cmd)
# remove temporary files
os.system("rm *.txt")