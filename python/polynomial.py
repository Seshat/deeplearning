
# To support both python 2 and python 3
from __future__ import division, print_function, unicode_literals

import numpy as np
import sys
import glob
import os
import drawBasics as db
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

def polyRegression(f,X,y,myDegree):   
    # poly_features = PolynomialFeatures(degree=myDegree, include_bias=False)
    # X_poly = poly_features.fit_transform(X)
    # lin_reg = LinearRegression()
    # lin_reg.fit(X_poly, y)
    X_new=np.linspace(-3, 3, 100).reshape(100, 1)
    # X_new_poly = poly_features.transform(X_new)
    # y_new = lin_reg.predict(X_new_poly)
    # db.drawPolyLine(f,X_new, y_new,'window.chartColors.green')

    polybig_features = PolynomialFeatures(degree=myDegree, include_bias=False)
    std_scaler = StandardScaler()
    lin_reg = LinearRegression()
    polynomial_regression = Pipeline([
            ("poly_features", polybig_features),
            ("std_scaler", std_scaler),
            ("lin_reg", lin_reg),
        ])
    polynomial_regression.fit(X, y)
    y_newbig = polynomial_regression.predict(X_new)
    f.write('              ,\n')
    f.write('              {\n')
    db.drawPolyLine(f,X_new, y_newbig,'"#cc65fe"')