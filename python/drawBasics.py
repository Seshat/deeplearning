import numpy as np
import os


def drawLine(f,a0,b0,a1,b1):
    f.write('              ,\n')
         
    f.write('                 {   \n') 
    f.write('                    type: "line",\n') 
    f.write('                    borderColor: "#ff6384",  \n')   
    f.write('                    fill: false,\n')
    f.write('          data: [\n')
    # At X=0 Y=b0
    f.write('            {\n')
    f.write('            x: 0,\n' % ( a0 ) )
    f.write('            y: %f\n' % ( b0) )
    f.write('            }, \n')
    f.write('            {\n')
    # At X=2 Y=b0+b1*2
    f.write('            x: %f,\n' % ( a1 ) )
    f.write('            y: %f\n' % ( b1) )
    f.write('            } \n')    
    f.write('		   ]\n')
    f.write('         }')


def header(f,name):
    f.write('    var %sData = { \n' % ( name))    

def onlyPoints(f,X,y):  
    f.write('        datasets: [\n')
    f.write('{\n')
    f.write('            pointRadius: 3,\n')
    f.write('            backgroundColor: "#36a2eb",\n')    
    f.write('            data: [\n')
    for i in range(1,len(X)-1):
        f.write('            {\n')
        f.write(' x: %f,\n' % ( X[i] ) )
        f.write('            y: %f\n' % ( y[i]) )
        f.write(' }, \n')
    f.write('            {\n')
    f.write(' x: %f,\n' % ( X[len(X)-1] ) )
    f.write('            y: %f\n' % ( y[len(X)-1]) )
    f.write(' } \n')    
    f.write('		]\n')
    f.write('                               }')

def drawPolyLine(f,X,y,color):
       
    # f.write('                 {   \n') 
    f.write('                    type: "line",\n') 
    f.write('                    borderColor: %s,  \n' % (color) )
    f.write('                    borderWidth:6,\n')
    f.write('                    pointRadius: 0,\n')
    f.write('                    fill: false,\n')
    f.write('          data: [\n')
    for i in range(1,len(X)-1):
        f.write('            {\n')
        f.write(' x: %f,\n' % ( X[i] ) )
        f.write('            y: %f\n' % ( y[i]) )
        f.write(' }, \n')
    f.write('            {\n')
    f.write(' x: %f,\n' % ( X[len(X)-1] ) )
    f.write('            y: %f\n' % ( y[len(X)-1]) )
    f.write(' } \n')    
    f.write('		]\n')
    f.write('                               }')

def footer(f):   
    f.write('                               ]\n')
    f.write('								};\n')   

def prepareChart(f,data,id,isLegend,xmin,xmax,ymin,ymax):
    f.write('window.myBar = new Chart(ctx, {\n')
    f.write('  type: "scatter",\n')
    f.write('  data: %sData,\n' % (data) ) 
    f.write('  options: {plugins: {\n')
    f.write('    responsive: true,\n')
    f.write('    legend: {\n')
    f.write('      display: %s,\n' % (isLegend) )
    f.write('      	labels: {\n')
    f.write('      	  fontSize: 22\n')
    f.write('      	},\n')
    f.write('      	position:"bottom"\n')
    f.write('        },\n')
    if ymax > 0:
        f.write('        scales: {\n')
        f.write('     yAxes: [{\n')
        f.write('         ticks: {\n')
        f.write('             min: %f,\n' % (ymin) )
        f.write('             max: %f\n' % (ymax))
        f.write('         }\n')
        f.write('     }],\n')
        if xmax > 0:
            f.write('     xAxes: [{\n')
            f.write('         ticks: {\n')
            f.write('             min: %f,\n' % (xmin))
            f.write('             max: %f\n' % (xmax))
            f.write('         }\n')
            f.write('     }]\n')        
        f.write(' }\n')
    f.write('  }}\n')
    f.write('});   \n')

def defineChart(f,data,id,isLegend,xmin,xmax,ymin,ymax):
    f.write('var ctx = document.getElementById("%s").getContext("2d");\n' % (id) )
    prepareChart(f,data,id,isLegend,xmin,xmax,ymin,ymax)

def prepareButton(f,data,id,isLegend,xmin,xmax,ymin,ymax,chart):    
    f.write('document.getElementById("%s").addEventListener("click", function() {\n' % (id) )
    f.write('const chart = Chart.getChart("%s");\n' % (chart) )
    f.write('chart.destroy();\n')
    f.write('var ctx = document.getElementById("%s").getContext("2d");\n' % (chart) )
    prepareChart(f,data,id,isLegend,xmin,xmax,ymin,ymax)
    f.write('});\n')