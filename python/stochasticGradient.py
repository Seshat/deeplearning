# To support both python 2 and python 3
from __future__ import division, print_function, unicode_literals

import numpy as np
import os
import drawBasics as db

theta_path_sgd = []
np.random.seed(42)

n_epochs = 50
t0, t1 = 5, 50  # learning schedule hyperparameters

def learning_schedule(t):
    return t0 / (t + t1)



def plot_stochastic_gradient(f,X,y,theta, X_b,X_new,X_new_b, theta_path=None):
    theta = np.random.randn(2,1)  # random initialization

    m = len(X_b)
    for epoch in range(n_epochs):
        for i in range(m):
            if epoch == 0 and i < 10:                    # not shown in the book
                y_predict = X_new_b.dot(theta)           # not shown
                db.drawLine(f,X_new[0],y_predict[0],X_new[1],y_predict[1])
            random_index = np.random.randint(m)
            xi = X_b[random_index:random_index+1]
            yi = y[random_index:random_index+1]
            gradients = 2 * xi.T.dot(xi.dot(theta) - yi)
            eta = learning_schedule(epoch * m + i)
            theta = theta - eta * gradients
            theta_path_sgd.append(theta)
    return theta_path_sgd