# To support both python 2 and python 3
from __future__ import division, print_function, unicode_literals

import numpy as np
import os
import drawBasics as db

np.random.seed(42)



def plot_gradient_descent(f,X,y,theta, eta,X_b,X_new,X_new_b, theta_path=None):

    n_iterations = 1000
    m = 100

    # for iteration in range(n_iterations):
    #     gradients = 2/m * X_b.T.dot(X_b.dot(theta) - y)
    #     theta = theta - eta * gradients

    m = len(X_b)
    # plt.plot(X, y, "b.")
    n_iterations = 12
    for iteration in range(n_iterations):
        if iteration < 10:
            y_predict = X_new_b.dot(theta)
            db.drawLine(f,X_new[0],y_predict[0],X_new[1],y_predict[1])
        gradients = 2/m * X_b.T.dot(X_b.dot(theta) - y)
        theta = theta - eta * gradients
        if theta_path is not None:
            theta_path.append(theta)

    return theta_path