############################################
#                                          #
#      Python script to create             #
#        figure with chart.js              #
#                                          #
############################################

# INPUT:
#########
# nothing

# OUTPUT:
#########
# chart....txt -> single chart
# final.html    -> Concatenation of the above 

import numpy as np
import sys
import glob
import os
import drawBasics as db
import gradientDescent as gd
import stochasticGradient as sg
import miniBashGradient as mbg
import polynomial
import learningCurve as lc
import regularization as regul

from sklearn.linear_model import Ridge
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures

from sklearn import datasets


## Compute some random points along X=Y
def computePoints():
    X = 2 * np.random.rand(100, 1)
    y = 4 + 3 * X + np.random.randn(100, 1)
    return [X,y]

## Compute the theta parameters based on the normal equation
def computeNormal():  
    theta_best = np.linalg.inv(X_b.T.dot(X_b)).dot(X_b.T).dot(y)
    y_predict = X_new_b.dot(theta_best)
    return [X_new ,y_predict]

def computepolyPoints():  
    m = 200
    X = 6 * np.random.rand(m, 1) - 3
    y = 0.5 * X**2 + X + 2 + np.random.randn(m, 1)
    return [X,y]

#######################################
# SCATTER POINTS AND NORMAL EQUATION
#######################################
## Chart 1 :scatter points
chartNumber=1
name='point'
f = open("charts1.txt", "w")
db.header(f,name)
[X,y]=computePoints()
X_b = np.c_[np.ones((100, 1)), X] # ajouter x0 = 1 à chaque obs.
X_new = np.array([[0], [2]])
X_new_b = np.c_[np.ones((2, 1)), X_new] # ajouter x0 = 1
db.onlyPoints(f,X,y)
db.footer(f) 
db.defineChart(f,name,'chart1','false',0,0,0,0)
f.close() 

## Chart 2 :normal equation
chartNumber=chartNumber+1
name='normal'
f = open("charts2.txt", "w")
db.header(f,name)
[X_new ,y_predict]=computeNormal()
db.onlyPoints(f,X,y)
# db.onlyLine(f)
db.drawLine(f,X_new[0],y_predict[0],X_new[1],y_predict[1])
db.footer(f) 
db.defineChart(f,name,'chart2','false',0,0,0,0)
db.defineChart(f,name,'chart2b','false',0,0,0,0)
db.defineChart(f,name,'chart2c','false',0,0,0,0)
f.close() 


#########################################
# GRADIENT DESCENT 
#########################################
## Chart 3, 4, 5 :gradient descend with different eta
theta = np.random.randn(2,1)

chartNumber=chartNumber+1
name='gd1'
eta = 0.02
f = open("charts3.txt", "w")
db.header(f,name)
db.onlyPoints(f,X,y)
gd.plot_gradient_descent(f,X,y,theta, eta,X_b,X_new,X_new_b)
db.footer(f)
# db.defineChart(f,name,'chart3','false',0,0,0,14)
db.prepareButton(f,name,'gd1','false',0,0,0,14,'chart4') 
f.close()

chartNumber=chartNumber+1
name='gd2'
eta = 0.1
f = open("charts4.txt", "w")
db.header(f,name)
db.onlyPoints(f,X,y)
theta_path_bgd = gd.plot_gradient_descent(f,X,y,theta, eta,X_b,X_new,X_new_b)
db.footer(f)
db.defineChart(f,name,'chart4','false',0,0,0,14)
db.prepareButton(f,name,'gd2','false',0,0,0,14,'chart4')
f.close()

chartNumber=chartNumber+1
name='gd3'
eta = 0.5
f = open("charts5.txt", "w")
db.header(f,name)
db.onlyPoints(f,X,y)
gd.plot_gradient_descent(f,X,y,theta, eta,X_b,X_new,X_new_b)
db.footer(f)
# db.defineChart(f,name,'chart5','false',0,0,0,14)
db.prepareButton(f,name,'gd3','false',0,0,0,14,'chart4') 
f.close()

#########################################
# EXOTIC GRADIENTS
#########################################

## Chart 6 : stochastic gradient
chartNumber=chartNumber+1
name='sg'
f = open("charts6.txt", "w")
db.header(f,name)
db.onlyPoints(f,X,y)
theta_path_sgd= sg.plot_stochastic_gradient(f,X,y,theta, X_b,X_new,X_new_b)
db.footer(f)
db.defineChart(f,name,'chart6','false',0,0,0,14)
f.close()

## Chart 7 : mini-bash gradient
chartNumber=chartNumber+1
name='mbg'
f = open("charts7.txt", "w")
db.header(f,name)
db.onlyPoints(f,X,y)
theta_path_mgd= mbg.plot_miniBash_gradient(f,X,y,theta, X_b,X_new,X_new_b)
db.footer(f)
db.defineChart(f,name,'chart7','false',0,0,0,14)
f.close()

#########################################
# POLYNOMIAL CURVE
#########################################

## Chart 8 :scatter points along poly
chartNumber=chartNumber+1
name='pointPoly'
f = open("charts8.txt", "w")
db.header(f,name)
[X,y]=computepolyPoints()
db.onlyPoints(f,X,y)
db.footer(f) 
db.defineChart(f,name,'chart8','false',0,0,0,14)
f.close() 

## Chart 9 : poly1
chartNumber=chartNumber+1
name='poly1'
f = open("charts9.txt", "w")
db.header(f,name)
db.onlyPoints(f,X,y)
polynomial.polyRegression(f,X,y,2)
db.footer(f) 
# db.defineChart(f,name,'chart9','false',0,0,0,14)
db.prepareButton(f,name,'deg2','false',0,0,0,14,'chart8') 
f.close() 

## Chart 10 : poly2
chartNumber=chartNumber+1
name='poly2'
f = open("charts10.txt", "w")
db.header(f,name)
db.onlyPoints(f,X,y)
polynomial.polyRegression(f,X,y,5)
db.footer(f) 
# db.defineChart(f,name,'chart10','false',0,0,0,14)
db.prepareButton(f,name,'deg5','false',0,0,0,14,'chart8') 
f.close() 

## Chart 11 : poly3
name='poly3'
chartNumber=chartNumber+1
f = open("charts11.txt", "w")
db.header(f,name)
db.onlyPoints(f,X,y)
polynomial.polyRegression(f,X,y,10)
db.footer(f)
# db.defineChart(f,name,'chart11','false',0,0,0,14) 
db.prepareButton(f,name,'deg10','false',0,0,0,14,'chart8') 
f.close() 

## Chart 12 : poly4
name='poly4'
chartNumber=chartNumber+1
f = open("charts12.txt", "w")
db.header(f,name)
db.onlyPoints(f,X,y)
polynomial.polyRegression(f,X,y,25)
db.footer(f) 
db.defineChart(f,name,'chart12','false',0,0,0,14)
db.prepareButton(f,name,'deg25','false',0,0,0,14,'chart8') 
f.close() 

## Chart 13 : poly5
name='poly5'
chartNumber=chartNumber+1
f = open("charts13.txt", "w")
db.header(f,name)
db.onlyPoints(f,X,y)
polynomial.polyRegression(f,X,y,75)
db.footer(f) 
db.defineChart(f,name,'chart13','false',0,0,0,14)
db.prepareButton(f,name,'deg75','false',0,0,0,14,'chart8') 
f.close() 


#########################################
# LEARNING CURVE
#########################################
[X,y]=computepolyPoints()
## Chart 14 : learning 1
chartNumber=chartNumber+1
name='learning1'
f = open("charts14.txt", "w")
db.header(f,name)
from sklearn.linear_model import LinearRegression
lin_reg = LinearRegression()
lc.plot_learning_curves(f,lin_reg,X,y,0.2)
db.footer(f) 
db.defineChart(f,name,'chart14','true',0,80,0,3)
db.defineChart(f,name,'chart14b','true',0,80,0,3)
f.close() 

## Chart 15 : learning 2
chartNumber=chartNumber+1
name='learning2'
f = open("charts15.txt", "w")
db.header(f,name)
lc.plot_learning_curves(f,lin_reg,X,y,0.8)
db.footer(f) 
db.defineChart(f,name,'chart15','true',0,80,0,3)
f.close() 

## Chart 16 : learning 3
chartNumber=chartNumber+1
name='learning3'
f = open("charts16.txt", "w")
db.header(f,name)
polynomial_regression = Pipeline([
  ("poly_features", PolynomialFeatures(degree=15, include_bias=False)),
  ("lin_reg", LinearRegression()),
])
lc.plot_learning_curves(f,polynomial_regression,X,y,0.2)
db.footer(f) 
db.defineChart(f,name,'chart16','true',0,80,0,3)
f.close() 


#########################################
# REGULARIZATION CURVE
#########################################
[X,X_new,y]=regul.computePoints()

## Chart 17 : regul 1
name='regul1'
chartNumber=chartNumber+1
f = open("charts17.txt", "w")
db.header(f,name)
regul.plot_model(f,X,X_new,y,Ridge, polynomial=False, alpha=0, color='"#ff6384"', random_state=42)
db.footer(f) 
db.defineChart(f,name,'chart17','false',0,0,0,0)
db.prepareButton(f,name,'alpha000','false',0,0,0,0,'chart17') 
f.close()

## Chart 18 : regul 2
chartNumber=chartNumber+1
name='regul2'
f = open("charts18.txt", "w")
db.header(f,name)
regul.plot_model(f,X,X_new,y,Ridge, polynomial=False, alpha=10, color='"#cc65fe"', random_state=42)
db.footer(f) 
# db.defineChart(f,name,'chart18','false',0,0,0,0)
db.prepareButton(f,name,'alpha010','false',0,0,0,0,'chart17') 
f.close() 
## Chart 19 : regul 3
chartNumber=chartNumber+1
name='regul3'
f = open("charts19.txt", "w")
db.header(f,name)
regul.plot_model(f,X,X_new,y,Ridge, polynomial=False, alpha=100, color='"#800080"', random_state=42)
db.footer(f) 
# db.defineChart(f,name,'chart19','false',0,0,0,0)
db.prepareButton(f,name,'alpha100','false',0,0,0,0,'chart17') 
f.close() 

## Chart 20 : regul 4
chartNumber=chartNumber+1
name='regul4'
f = open("charts20.txt", "w")
db.header(f,name)
regul.plot_model(f,X,X_new,y,Ridge, polynomial=True, alpha=0, color='"#ff6384"', random_state=42)
db.footer(f) 
db.defineChart(f,name,'chart20','false',0,0,0,0)
db.defineChart(f,name,'chart20b','false',0,0,0,0)
db.prepareButton(f,name,'alpha00','false',0,0,0,0,'chart20') 
f.close() 

## Chart 21 : regul 5
chartNumber=chartNumber+1
name='regul5'
f = open("charts21.txt", "w")
db.header(f,name)
regul.plot_model(f,X,X_new,y,Ridge, polynomial=True, alpha=10**-5, color='"#cc65fe"', random_state=42)
db.footer(f) 
db.defineChart(f,name,'chart21','false',0,0,0,0)
db.prepareButton(f,name,'alpha05','false',0,0,0,0,'chart20') 
f.close() 

## Chart 22 : regul 6
chartNumber=chartNumber+1
name='regul6'
f = open("charts22.txt", "w")
db.header(f,name)
regul.plot_model(f,X,X_new,y,Ridge, polynomial=True, alpha=1, color='"#800080"', random_state=42)
db.footer(f) 
# db.defineChart(f,name,'chart22','false',0,0,0,0)
db.prepareButton(f,name,'alpha01','false',0,0,0,0,'chart20') 
f.close() 

def sigmoid(z):
    return 1 / (1 + np.exp(-z))

#########################################
# SIGMOID
#########################################
## Chart 23 : sigm
chartNumber=chartNumber+1
name='sigm'
f = open("charts23.txt", "w")
t = np.linspace(-10, 10, 100)
sig=sigmoid(t)
db.header(f,name)
f.write('           datasets: [\n')
f.write('              {\n')
db.drawPolyLine(f,t,sig,'"#ff6384"')
db.footer(f) 
db.defineChart(f,name,'chart23','false',0,0,0,0)
f.close() 


#########################################
# Linear on Poly
#########################################
## Chart 22 bis : poly0
chartNumber=chartNumber+1
f = open("charts24.txt", "w")
name='poly'
db.header(f,name)
[X,y]=computepolyPoints()
db.onlyPoints(f,X,y)
polynomial.polyRegression(f,X,y,1)
db.footer(f) 
db.defineChart(f,name,'chart24','false',0,0,0,0)
f.close() 

#########################################
# RÉGRESSION LOGISTIQUE
#########################################
chartNumber=chartNumber+1
f = open("charts25.txt", "w")
name='logi1'
db.header(f,name)
iris = datasets.load_iris()
X = iris["data"][:, 3:]  # petal width
y = (iris["target"] == 2).astype(np.int)  # 1 if Iris-Virginica, else 0
from sklearn.linear_model import LogisticRegression
log_reg = LogisticRegression(solver="liblinear", random_state=42)
log_reg.fit(X, y)
X_new = np.linspace(0, 3, 1000).reshape(-1, 1)
y_proba = log_reg.predict_proba(X_new)
decision_boundary = X_new[y_proba[:, 1] >= 0.5][0]
print ('echo')
print (decision_boundary)
# f.write('           datasets: [\n')
db.onlyPoints(f,X[y==1], y[y==1])
f.write('              ,{\n')
f.write('           label: "Iris-Virginica",\n')
db.drawPolyLine(f,X_new,y_proba[:, 1],'"#800080"')
f.write('              ,{\n')
f.write('           label: "pas Iris-Virginica",\n')
db.drawPolyLine(f,X_new,y_proba[:, 0],'"#ff6384"')
f.write('              ,{\n')
f.write('           label: "Frontiere de decision",\n')
db.drawPolyLine(f,[decision_boundary, decision_boundary, decision_boundary], [-1,-1, 2],'"#cc65fe"')
db.footer(f) 
db.defineChart(f,name,'chart26','true',0,0,0,0)
f.close() 

# Prepare the html file
# ##########################################
#Concatenate the variable and the scene files
print(chartNumber)
# os.system("cat header.html charts1.txt > temp1.txt")
os.system("cat charts1.txt > temp1.txt")
for cpt in range(1, chartNumber):
    cmd="cat temp"+str(cpt)+".txt charts"+str(cpt+1)+".txt  > temp"+str(cpt+1)+".txt"
    os.system(cmd)
    print(cmd)
cmd="cp temp"+str(chartNumber)+".txt ../javascript/scriptAI1.js"  
os.system(cmd)
# cmd="cat temp"+str(chartNumber)+".txt footer.html  > final.html"    
# os.system(cmd)
print(cmd)
# remove temporary files
os.system("rm *.txt")