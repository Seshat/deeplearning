
import numpy as np
import os
import drawBasics as db


from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

lin_reg = LinearRegression()

def computePoints():
    np.random.seed(42)
    m = 20
    X = 3 * np.random.rand(m, 1)
    y = 1 + 0.5 * X + np.random.randn(m, 1) / 1.5
    X_new = np.linspace(0, 3, 100).reshape(100, 1)
    return [X,X_new,y]

def plot_model(f,X,X_new,y,model_class, polynomial, alpha, color, **model_kargs):
    [X,X_new,y]=computePoints()
    # f.write('           datasets: [\n')
    db.onlyPoints(f,X,y)
    f.write('              ,\n')

    model = model_class(alpha, **model_kargs) if alpha > 0 else LinearRegression()
    if polynomial:
        model = Pipeline([
                ("poly_features", PolynomialFeatures(degree=10, include_bias=False)),
                ("std_scaler", StandardScaler()),
                ("regul_reg", model),
            ])
    model.fit(X, y)
    y_new_regul = model.predict(X_new)
    # if alpha != alphas[-1]:
    f.write('              {\n')
    db.drawPolyLine(f,X_new, y_new_regul,color)
    f.write('              ,\n')