# Explanation on the pythons scripts

# Code execution
All the python codes could be executed with `executeAllPythonCode.sh`

Otherwise, each example part of the course could be executed separately:

- `scriptAI1.py`: regression-based machine learning
- `scriptAI2.py`: Perceptron 
- `scriptAI3.py`: Unused yet
- `scriptAI4.py`: Unused yet

# Utilities

Various function are called in the code:

- `drawBasics.py`: drawing tools (point, line, etc)
- `gradientDescent.py`: Compute regression with gradient descent
- `learningCurve.py`: Compute learning curves
- `miniBashGradient.py`: Compute regression with mini Bash Gradient
- `polynomial.py`: Compute regression on polynomial model
- `regularization.py`: Compute regression with a given regularization coeficient
- `stochasticGradient.py`: Compute regression with stochastic gradient