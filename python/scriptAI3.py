############################################
#                                          #
#      Python script to create             #
#        figure with chart.js              #
#                                          #
############################################

# INPUT:
#########
# nothing

# OUTPUT:
#########
# chart....txt -> single chart
# final.html    -> Concatenation of the above 

import numpy as np
import sys
import glob
import os
import drawBasics as db
import gradientDescent as gd
import stochasticGradient as sg
import miniBashGradient as mbg
import polynomial
import learningCurve as lc
import regularization as regul

from sklearn.linear_model import Ridge
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures

## Compute some random points along X=Y
def computePoints():
    X = 2 * np.random.rand(100, 1)
    y = 4 + 3 * X + np.random.randn(100, 1)
    return [X,y]

## Compute the theta parameters based on the normal equation
def computeNormal():  
    theta_best = np.linalg.inv(X_b.T.dot(X_b)).dot(X_b.T).dot(y)
    y_predict = X_new_b.dot(theta_best)
    return [X_new ,y_predict]

def computepolyPoints():  
    m = 200
    X = 6 * np.random.rand(m, 1) - 3
    y = 0.5 * X**2 + X + 2 + np.random.randn(m, 1)
    return [X,y]

#######################################
# SCATTER POINTS AND NORMAL EQUATION
#######################################
## Chart 1 :scatter points
chartNumber=1
name='point'
f = open("charts1.txt", "w")
db.header(f,name)
[X,y]=computePoints()
X_b = np.c_[np.ones((100, 1)), X] # ajouter x0 = 1 à chaque obs.
X_new = np.array([[0], [2]])
X_new_b = np.c_[np.ones((2, 1)), X_new] # ajouter x0 = 1
db.onlyPoints(f,X,y)
db.footer(f) 
db.defineChart(f,name,'chart1','false',0,0,0,0)
f.close() 

# Prepare the html file
# ##########################################
#Concatenate the variable and the scene files
print(chartNumber)
# os.system("cat header.html charts1.txt > temp1.txt")
os.system("cat charts1.txt > temp1.txt")
for cpt in range(1, chartNumber):
    cmd="cat temp"+str(cpt)+".txt charts"+str(cpt+1)+".txt  > temp"+str(cpt+1)+".txt"
    os.system(cmd)
    print(cmd)
cmd="cp temp"+str(chartNumber)+".txt ../javascript/scriptAI3.js"  
os.system(cmd)
# cmd="cat temp"+str(chartNumber)+".txt footer.html  > final.html"    
# os.system(cmd)
print(cmd)
# remove temporary files
os.system("rm *.txt")